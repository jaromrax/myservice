#!/usr/bin/python3

import zmq
import time

# ZeroMQ Context
context = zmq.Context()

# Define the socket using the "Context"
sock = context.socket(zmq.REP)
sock.bind("tcp://127.0.0.1:5678")
print("port 5678 bound")
# Run a simple "Echo" server
i=0
while True:
    i=i+1
    print(i,"receiving")
    message = sock.recv_json()
    sock.send_json( {} )
    print( message )
    #time.sleep(0.1)
    
