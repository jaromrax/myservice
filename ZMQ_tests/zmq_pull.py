#!/usr/bin/python3

import time
import zmq
import pprint

def result_collector():
    context = zmq.Context()
    results_receiver = context.socket(zmq.PULL)
    results_receiver.bind("tcp://127.0.0.1:5558")
    print("ZMQ PULL bound to port 5558")
    collecter_data = {}
    x=0
    while True:
        #for x in range(1000):
        x=x+1
        result = results_receiver.recv_json()
        #print( result)
        if result['consumer'] in collecter_data.keys():
        #if collecter_data.has_key(result['consumer']):
            collecter_data[result['consumer']] = collecter_data[result['consumer']] + 1
        else:
            collecter_data[result['consumer']] = 1
        if x%100==0:
            pprint.pprint(collecter_data)
            print(result)

result_collector()
