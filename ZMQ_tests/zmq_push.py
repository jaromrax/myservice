#!/usr/bin/python3
import time
import zmq
import random

# def producer():
#     context = zmq.Context()
#     zmq_socket = context.socket(zmq.PUSH)
#     zmq_socket.bind("tcp://127.0.0.1:5557")
#     print("PUSH bound ")
#     # Start your result manager and workers before you start your producers
#     for num in xrange(20000):
#         work_message = { 'num' : num }
#         zmq_socket.send_json(work_message)

def consumer():
    consumer_id = random.randrange(1,10005)
    print( "I am consumer #%s and PUSHer and I throw different messages to different PULLers" % (consumer_id))
    context = zmq.Context()
    # recieve work
    #consumer_receiver = context.socket(zmq.PULL)
    #consumer_receiver.connect("tcp://127.0.0.1:5557")
    # send work
    consumer_sender = context.socket(zmq.PUSH)
    consumer_sender.connect("tcp://127.0.0.1:5558")

    i=0
    while True:
        time.sleep(0.01)
        i=i+1
        print( "I PUSH #%s  %d" % (consumer_id,i))
        #work = consumer_receiver.recv_json()
        work={'num':i,'age':99,'consumer':2018}
        data = work['num']
        result = { 'consumer' : consumer_id, 'num' : data}
        #if data%2 == 0: 
        consumer_sender.send_json(result)

consumer()

#producer()
