#!/usr/bin/python3

print("DEALER - REP is async broadcast to many REP")

import zmq
import time
import sys
import threading
import uuid
SOCKET_NAME = "tcp://127.0.0.1:8000"
#SOCKET_NAME = "inproc://mysocket"

def dealerRoutine(context):
    socket = context.socket(zmq.DEALER)
    #socket.identity = uuid.uuid4().hex[:4]
    socket.bind(SOCKET_NAME)
    i=0
    while True:
        i=i+1
        time.sleep(0.7)
        socket.send_string("", zmq.SNDMORE) # must be
        work= {'a':i, 'b':2}
        socket.send_json( work )  # real send
        print("d: sent: ", work)
    #socket.send("", zmq.SNDMORE)
    #socket.send("hello")
    #print("d: sent", socket.recv())
    #print("d: sent", socket.recv())
    socket.close()


def workerRoutine(context, ide):
    print("worker setup:")
    socket = context.socket(zmq.REP)
    socket.connect(SOCKET_NAME)
    print(ide,"worker connected:")
    while True:
        
        if socket.poll(100):
            #print("      w: receiving...")
            jid = socket.recv_json()
            print("      w",ide,": received", jid)
            socket.send_json( {'result':'ok'} )


    
context = zmq.Context()

worker = threading.Thread(target=workerRoutine, args=([context,1]))
worker.start()
worker2 = threading.Thread(target=workerRoutine, args=([context,2]))
worker2.start()

dealerRoutine(context)

context.term()
