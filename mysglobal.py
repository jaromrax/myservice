#!/usr/bin/env python3

#############
#   this is to be leaded by every module.
#  I think
#import mysglobal as g # args,loggerr @every module
#################

#import logging
#from logzero import setup_logger,LogFormatter,colors
import logzero
import argparse

import os,sys
import json
from blessings import Terminal

import getpass  #  lockfile<=  getuser

#from threading import Thread  # thread: i need accesible thread
import uuid
import subprocess as s
DEBUG=True
config={}  # global config, but not sure

MYSEPATH=os.path.expanduser("~/.myservice")
MYSECONFIG = os.path.expanduser("~/.myservice.conf")

I_AM_INFINITE=False
BOTTOMLINE_TEXT="no message"
t = Terminal()

ZMQ_REP_PORT=5678

RANDOM_STR = uuid.uuid4()


# docker has not $USER
#user_name = os.getenv('USER')  # for /var/run/screen/S-user
user_name = s.check_output( "/usr/bin/whoami" ).decode("utf8").rstrip()

print( user_name,"!!!" )

####################################
# PARSER ARG
######################################
parser=argparse.ArgumentParser(description="""
------------------------------------------------------------------
 The tool to run services in userspace
""",usage="""
myservice  [-d]            ... shows the executables in ~/.myservice
myservice  [-d] infinite   ... run infinite (in terminal)

myservice        test         ...  test is without a path inside ~/.myservice
myservice  [-d] test enable  ... introduces into ~/.myservice.config
myservice  [-d] test disable
myservice  [-d] test never   ... gray servicename and mode
myservice  [-d] test undef
myservice  [-d] test start
myservice  [-d] test stop    ... kills and makes UNDEF
myservice  [-d] test perm4h  ... run every 4 hours (it knows m,h,d)

myservice  [-d]  reconfig    ...  when MANUAL edit to .confg.json is done

script /dev/null              ...  do this when an ssh user without access to screen
------------------------------------------------------------------
VARIOUS TRICKS:
   myservice    ...  looks for all executables;
       *      ... already present in .myservice.conf
       E      ... atribute enable is there
       + or - ... attribute enable is true or false
       p      ... attribute perm is ON;  also a,x
   PATHS:
       when ~/.myservice/test/aaa
       myservice aaa enable : finds a path and adds into the .myservice.conf
   myservice infinite ... runs the table in the terminal (only 1 instance possible)
                           OR connects to the screen -x myservice_infinite
""",
 formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('-d','--debug', action='store_true' , help='')
#parser.add_argument('-s','--serfmsg', default='',nargs="+" , help='serf message to mmap') # list will come after

#parser.add_argument('count', action="store", type=int)
parser.add_argument('service', action="store", nargs="?") # nargs='+' :
parser.add_argument('command', action="store", nargs="?") # nargs='+'
#parser.add_argument('command', action="store")

# print("""
# USAGE CASES:
# ------------------------------------------------------------------
# ./myservice.py  -d infinite
# ./myservice.py   test enable
# ------------------------------------------------------------------
# VARIOUS TRICKS:
#    subdir TTT
#    myservice TTT/aaa enable : adds into the config
#    #  this was the last time about PATH!; from now on:
#    myservice aaa   sock

#   ./myservice.py -s myservice aaa    # send command to mmap to test serf
#   #       aaa must be status   sock


# """)

args=parser.parse_args()

#=========== path must exist
if not os.path.isdir(  os.path.expanduser("~/.myservice")  ):
    #print("  directory exists")
#else:
    print("  DIR NOT EXISTS")
    os.mkdir( os.path.expanduser("~/.myservice")  )


###########################################
# LOGGING   - after AGR PARSE
########################################
log_format = '%(color)s%(levelname)1.1s... %(asctime)s%(end_color)s %(message)s'  # i...  format
#logzero.LogFormatter.DEFAULT_COLORS[10] = logzero.colors.Fore.YELLOW ## debug level=10. default Cyan...
loglevel=1 if args.debug==1 else 11  # all info, but not debug
formatter = logzero.LogFormatter(fmt=log_format,datefmt='%Y-%m-%d %H:%M:%S')
logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename(sys.argv[0]) )[0]+'.log'
logger = logzero.setup_logger( name="main",logfile=logfile, level=loglevel,formatter=formatter , maxBytes=1e6, backupCount=3)#to 1-50






lockfile="/tmp/"+"myservice_"+getpass.getuser()+".lock"

lockfilepid=0
