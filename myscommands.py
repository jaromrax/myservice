#!/usr/bin/env python3

import mysglobal as g # args,loggerr @every module

import mysscreen
import mysconfig
import datetime
import time # to wait around USR1

import os # for kill  USR2 read
import signal

def myservice_get_status(service):
    li=mysscreen.screen_ls()
    names=[ i.split(".myservice_")[1].split()[0] for i in li]
    g.logger.info("SERVICE: ... ... "+service)
    notrun=False
    if service in names:
        g.logger.info("status   ... ... RUNNING")
    else:
        g.logger.info("status   ... ... NOT running")
        notrun=True
    #g.config=mysconfig.read_myservice_config()
    if service in g.config.keys():
        if notrun:
            g.logger.info("LAST  :  ... "+g.config[service]['lastrun'])
        g.logger.info("STATUS:  ... "+g.config[service]['status'])
    else:
        g.logger.info("         ... ... NOT in " +g.MYSECONFIG)





def myservice_set_status( service , status ):
    """
    used when  STOP is required
    also  when 'perm' - it saves
    WE HAVE 2 DIFFERENT SAVES:   1-actual status in INFINITE
                                 2-the change offline
                                 3- force read in INFINITE
    """
    g.logger.debug("changing service status:"+status)

    #g.config=read_myservice_config()
    if service in g.config.keys():
        g.config[service]['status']=status
        mysconfig.save_myservice_config()
        os.kill( g.lockfilepid, signal.SIGUSR2 ) # force infinite to read
    else:
        g.logger.error("no service {}".format(service) )




#################### OFFLINE COMMANDS #######
#
#  some of them should
#      -SAVE (maybe through SIGUSR1)
#      -and invoke READ config   (SIGUSR1)
#
# ENABLE DISABLE PERM
#
#  now: enhance with PATH possibility
def myservice_enable( service ):
    #g.config=read_myservice_config()
    if service in g.config.keys(): # if is already in keys
        fullpath=g.MYSEPATH
        if g.config[service]["path"]!="":
            fullpath=fullpath+"/"+g.config[service]["path"]+"/"
        if (not os.path.isfile(fullpath+service)) or (not  os.access(fullpath+service, os.X_OK)):
            g.logger.error("NOT PRESENT or NOT EXECUTABLE")
            return
        g.config[service]['enabled']=True
        g.logger.debug("enabled, now save directly:")
        mysconfig.save_myservice_config(  )
        g.logger.debug("kill usr2 - provoke READ in infinite:")
        os.kill( g.lockfilepid, signal.SIGUSR2 ) # force infinite to read

    else:
        g.logger.error("no service {} in  {} ... ".format(service, g.MYSECONFIG) )
        found=False
        for i in sorted(mysconfig.get_list_myservice_files() ):
            g.logger.debug( "service: "+ i  )
            if len(i.split("/"))>1:
                iexe=i.split("/")[-1]
                ipath=i.split("/")[0]
            else:
                iexe=i
                ipath=""
            if service==iexe:
                g.logger.debug("service     found in the list "+service+" "+ipath)
                ipathFIX=ipath
                found=True
        if not found:
            g.logger.error("Service not found or not executable, quit")
            return
        TIMEFMT="%Y/%m/%d-%H:%M:%S"
        TIMESTR=datetime.datetime.now().strftime( TIMEFMT )
        mysconfig.introduce_empty_member( service , TIMESTR ) # enabled by default
        g.config[service]['enabled']=True
        g.config[service]['path']=ipathFIX

        g.logger.info("new service introduced; save directly:")
        time.sleep(1) # I have crashes of infinite when savim perm1m
        mysconfig.save_myservice_config(  )
        time.sleep(1)
        g.logger.debug("kill usr2 - provoke READ in infinite:")
        os.kill( g.lockfilepid, signal.SIGUSR2 ) # force infinite to read





def myservice_disable( service ):
    #g.config=read_myservice_config()
    if service in g.config.keys():
        g.config[service]['enabled']=False
        mysconfig.save_myservice_config(  )
        g.logger.debug("disable before kill usr2")
        os.kill( g.lockfilepid, signal.SIGUSR2 ) # force infinite to read

    else:
        g.logger.error("no service {}".format(service) )





# i dont know: kill sure. but if perm??? should stop forever
def myservice_stop( service  ):
    g.logger.debug("service screen operation stop")
    #g.config=read_myservice_config()
    if service in g.config.keys():
        mysscreen.screen_kill( service )
    else:
        g.logger.error("no service {}".format(service) )




def myservice_start( service , dontrun="" , param=""):
    """ calls screen_new
    checks config file and ONLY  enabled      can run
    checks screen_ls for RUNNING
    - only not running CAN GO
    - Also used to start SOCKET services
         - dontun="perm" means, avoid perm from socket run
         - at possible to call
         - undef possible to call
    """
    g.logger.debug("myservice_start ... screen operation begins")
    #g.config=read_myservice_config()
    if service in g.config.keys() and g.config[service]['enabled']:
        # check if running
        li=mysscreen.screen_ls()
        names=[ i.split(".myservice_")[1].split()[0] for i in li]
        # do not run twice
        if service in names:
            g.logger.error("NEW: "+service+" already running, no action")
            return

        if g.config[service]['status']!=dontrun:
            g.logger.debug("screen_new ... {} / {}".format(service,param) )
            mysscreen.screen_new( service , param)
        else:
            g.logger.error("from myservice.start {}: forbiden to run {}".format(service,dontrun) )
    else:
        g.logger.error("from myservice.start: no service {}".format(service) )
