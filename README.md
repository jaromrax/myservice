# myservice

This is an attempt to combine `service` or `systemctl` and `cron` in some way together.


## Installation:

Run `make1file`. It will
- create single file
- recommend to copy it into `/usr/local/bin`
- recommend to update/create `/etc/rc.local`

## Running:

`myservice infinite`

## Operate:

 The scripts to run are under `$HOME/.myservice/` directory - from now on they will be called services.


Then - every 20 seconds it goes through the services and runs those defined to run.



### Basic flags - enabling

- `myservice` - list the services (executable files in ~/.myservice/)

*filenames with . ~ or # are not displayed/taken into account*

- `myservice enable` - will put service into .myservice.conf and set enable flag. The service then will be listed in *infinite* screen.

- `myservice disable` - will set off the enable flag in .myservice.conf




### Basic flags - run
There are three fields: running / permanent / onstartup

- running - detects a presence of screen with the service

- permanent - every 20 seconds, in case the service is not running in screen, it starts it up

- onstartup - the service is started at the very first run of /usr/bin/myservice infinite. This flag can coexist with the permanent flag.


### Commandline options
myservice progname { start | startin | enter | stop | restart | permanent | onstartup}

start - this starts `screen` with the service

startin - start and enter screen with screen -x

enter - enter the service in screen

stop - stop the service and remove the flag permanent

restart -

permanent - set the flag (perm perma also valid)

onstartup - set the flag

*remember that this is an early alpha, be very careful*




## Network hook:  nczmq.py

  *myservice uses zmq to communicate json messages and start applications
  on a signal* For this `nczmq.py` was created.


### REP - REQ

 *peer to peer scheme. Some confirmation necessary. `myservice` server is REP and waits on port 5678 (or 5679... if occupied by another user)*.  `nczmq.py` sends REQ packet with json information. Example::
 ```
 {'n': 1, 'worker': 0, 'message': 'ahoj', 'time': '14:02:30', 'run': 'callme', 'parameter': '', 'from': 'ojr@edie18'}
 ```

Service `callme` with no parameter will be called/started (if exists, enabled and is `sock` type).

Real example- run notify.py with a parameter:
```
 ./nczmq.py -d -m ahojky -s notify.py -p ahojky                                        [15:26:56]
TARGETS== ['127.0.0.1:5678']
Connecting to machine... 127.0.0.1:5678
socket connected to 127.0.0.1:5678
=================================================
1 -----Sending REQ --- to 127.0.0.1:5678 -------
   {'n': 1, 'worker': 0, 'message': 'ahojky', 'time': '15:27:07', 'run': 'notify.py', 'parameter': 'ahojky', 'from': 'ojr@edie18'}
1 0     +waiting response...
1 0     Received reply  0 [ {} ]
```


### PUB - SUB

 *publisher-subscriber scheme. No wait for confirmations, fire and forget.
 Used for flask websites. Not implemented now.*


## Appendix

### Translation to a single file


https://stackoverflow.com/questions/9002275/how-to-build-a-single-python-file-from-multiple-scripts

- use `stickytape ms2.py --add-python-path . > /tmp/myservice`

- over problem with `/usr/bin/env python3` in shebang

### Initial startup

The program should be started in /etc/rc.local by su - user -c "/usr/bin/myservice infinite"
