#!/usr/bin/env python3
import mysglobal as g # args,loggerr @every module

import os
import subprocess as s

import datetime

import sys

def get_services_from_screen_ls( li ):
    """ argument:  list directly from screen -ls ... wild mess
        returns:   clean list on names (no paths)
"""
    lo=[]
    for i in li:
        # RETRIEVE all data from running screen:
        name=i.split(".myservice_")[1].split()[0]
        lo.append(name)
        #session=i.strip().split()[0]
        #startup=i.strip().split("(")[1]
        #startup=startup.split(")")[0]
        # decode time from SCREEN ()
    return lo






###########################
def screen_ls(  ):
    """runs   scren -ls
    returns list of services found
    """
    # li=os.listdir( "/var/run/screen/S-"+g.user_name)
    # li=[ i for i in li if i.find('.myservice_')>0]
    # li=[ i for i in li if i.find('.myservice_infinite')<0]
    # li=[ i for i in li if i.find('.myservice_infinite')<0]
    # return li

    #  DATE FORMAT IS IMPORTAMT
    new_env = dict( os.environ )
    new_env['LC_TIME'] = 'en_US.UTF-8'
    new_env['LC_ALL'] = 'en_US.UTF-8'  # this solved am pm
    # raspian 8 returns always 1:
    #CMD="screen -ls" # NORMAL WAY
    CMD="screen -ls; exit 0" # RPi8
    # res=s.check_output( CMD.split() ,env=new_env ) # output:exception on 1
    success=True
    #g.logger.debug("trying to run screen -ls; exit 0 (RPi8 solution)")
    try:
        #res=s.check_output( CMD.split() ,env=new_env ) # NORMAL WAY
        res=s.check_output( CMD ,env=new_env , shell=True) #RPi8
    except s.CalledProcessError:
        success=False
        g.logger.error(" NO SCREENS ... screen -ls failed")
    if not success: return []
    li=res.decode('utf8').rstrip().split("\n")
    if li==[]: return []
    # only myservice screens
    li=[ i for i in li if i.find('.myservice_')>0]
    # remove infinite
    li=[ i for i in li if i.find('.myservice_infinite')<0]
    # replace back from the slash ..... NOT EFFECTIVE NOW! Rm
    li=[ i.replace("^","/") for i in li ]
    #logger.debug( li )
    #
    #  clean what comes out of "li"
    #
    linames=get_services_from_screen_ls( li )
    g.logger.debug("screen -ls ...         " + str( len(li))+" screens/"+ str(len(set(linames)))+" myservice"  )
    return li
    # LIST OF ALL RUNNINNG : dirty lines!!!!!





def screen_ls_infinite():
    """ makes screen -ls
        returns True if  myservice_infinite seen
"""

    g.logger.debug("check for myservice_infinite;screen-ls;exit0;")
    new_env = dict( os.environ )
    new_env['LC_TIME'] = 'en_US.UTF-8'
    new_env['LC_ALL'] = 'en_US.UTF-8'  # this solved am pm
    CMD="screen -ls"
    CMD="screen -ls; exit 0"
    try:
        #res=s.check_output( CMD.split() ,env=new_env )
        res=s.check_output( CMD ,env=new_env , shell=True )
        #g.logger.debug(" just  did run screen ls; exit 0")
    except:
        g.logger.error("NO SCREENS at all after screen -ls")
        res=b''
    li=res.decode('utf8').rstrip().split("\n")
    # only myservice screens
    li=[ i for i in li if i.find('.myservice_infinite')>0]
    if len(li)>0:
        g.logger.info(" ... myservice_infinite SCREEN  already running")
        return True
    return False
    # LIST OF ALL RUNNINNG : dirty lines!!!!!






def screen_enter( service ):
    """
    only runs     screen -x  with bash
    no checks, nothing
    BUT check the myservice_infinite
    """
    g.logger.info("SCREEN ENTER-----------: "+service)
    if service=="myservice_infinite":
        CMD="/usr/bin/screen -x "+service
    else:
        linames=get_services_from_screen_ls( screen_ls( ) )
        if not service in linames:
            g.logger.error(service+" NOT FOUND in screen -ls")
            sys.exit(1) #quit()
        CMD="/usr/bin/screen -x myservice_"+service
    g.logger.debug( CMD )
    #res=s.check_output( CMD.split() )
    res=s.check_call( CMD.split() )
    g.logger.debug( "       result of check_call:" + str(res) )






def screen_new( service , parameter=""  ):
    """
    only runs     screen -dmS  with bash
    no checks, nothing
    """
    g.logger.debug("SCREEN NEW-----------service: "+service)
    g.logger.debug("SCREEN NEW-----------paramet: "+parameter)
    #g.MYSEPATH= os.path.expanduser("~/.myservice")# IN GLOBALS
    MYSEPATH=g.MYSEPATH
    if 'path' in g.config[service] and g.config[service]['path']!="":
        MYSEPATH=g.MYSEPATH+"/"+g.config[service]['path']
    os.chdir( MYSEPATH ) # get to the service directory
    # and
    if not os.path.isfile(service) or not  os.access(service, os.X_OK):
        g.logger.error(service+" NOT PRESENT/EXECUTABLE")
        sys.exit(1) # quit()
    #g.logger.debug("i am in    "+MYSEPATH)
    g.logger.debug("i am in dir:"+os.getcwd() )
    # I REMOVE ALL "" AROUND
    # I REPLACE ALL ; (telldus has it, shell hates it)
    if parameter=="":
        parameter2=""
    else:
        # prepare parameter carefully:
        #parameter2=parameter.strip('"').split()[0]
        parameter2=parameter.strip('"')
        parameter2=parameter2.replace(" ","\ ")
        parameter2=parameter2.replace(";","\;")
        parameter2=parameter2.replace(",","\,")
    CMD='/usr/bin/screen -dmS myservice_'+service+' bash -c "./'+service+' '+parameter2+'"'
    CMD=CMD.rstrip()
    g.logger.debug( CMD )
    #res=s.check_output( CMD.split() )
    #res=s.check_call( CMD.split() ) # worked withou parameters
    res=s.check_call( CMD, shell=True )
    g.logger.debug( "       result of check_call:" + str(res) )
    g.config[service]['age']=""
    g.config[service]['lastrun']=datetime.datetime.now().strftime("%Y/%m/%d-%H:%M:%S")





def screen_kill( service ):
    g.logger.error("SCREEN KILL--------------------------: "+service)
    li=screen_ls()
    names=[ i.split(".myservice_")[1].split()[0] for i in li]
    if not service in names:
        g.logger.error("STOP: "+service+" already not running , no action")
        return
    # dockerdown should be a reference to a script like dborg.down
#    if "desc" in g.config[service]:
#        g.logger.error( "DESCRIPTION PRESENT " )
    if "dockerdown" in g.config[service]:
        g.logger.error( "DOCKERDOWN PRESENT " )
        MYSEPATH=g.MYSEPATH
        if 'path' in g.config[service] and g.config[service]['path']!="":
            MYSEPATH=g.MYSEPATH+"/"+g.config[service]['path']
        g.logger.error( MYSEPATH )
        os.chdir( MYSEPATH ) # get to the service directory
        CMD="./"+g.config[service]['dockerdown'].strip()
        g.logger.error( CMD )
        g.logger.debug( CMD )
        res=s.check_call( CMD.split() )
        g.logger.debug( "    result of check_call KILL:"+str(res) )
        return

    g.logger.debug( "DOCKERDOWN *NOT* PRESENT " )
    CMD="/usr/bin/screen -X -S myservice_"+service+" kill"
    g.logger.debug( CMD )
    res=s.check_call( CMD.split() )
    g.logger.debug( "    result of check_call KILL:"+str(res) )
    #---I addded 20200615 to compensate nonsenses...
    g.config[service]['lastrun']=datetime.datetime.now().strftime("%Y/%m/%d-%H:%M:%S")
