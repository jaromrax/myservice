#!/usr/bin/env python3

import mysversion
import subprocess as sp  # for Popen nohup
import sys

print("COMMIT FROM {}".format(mysversion.vdate) )


#---  this enables to check who needs to be imported
#--- and also import autmatic
import importlib

#allmods=['zmq','shutil','signal','logging','logzero','argparse','blessings','terminaltables','colorclass','threading','getpass','uuid','socket','gevent','json']
# ubu 20.04
allmods=['stickytape', 'zmq','logging','logzero',
         'argparse','blessings','terminaltables',
         'colorclass','uuid','gevent']


#==========================================install check_call======
def install(package):
    CMD = [sys.executable, "-m", "pip", "install", package]
    print(CMD)
    sp.check_call(CMD)

def checkmod( module ):
    modchk = importlib.util.find_spec(module)
    if modchk is None:
        #if module=="zmq":
        #    module="pyzmq"
        print("X... ",module,"module NOT FOUND, try pip3 install",module)
        print("yes?")
        if input().find("y")!=0:
            sys.exit(1)
        try:
            importlib.import_module(module)
        except ImportError:
            install(module)
        finally:
            globals()[module] = importlib.import_module(module)

        sys.exit(1)
    else:
        #print("i...",s,'[OK]')
        print(".",end="")




print("i... modules check:", end="")
for i in allmods:
         checkmod( i)
print("\n")






import os
import sys
import signal
import time

import mysglobal as g #  args, logger @every module

import mysconfig #  saving data on signal

import zmq # to kill zmq via REQ

g.logger.info("==================================================")

# I think now they would know logger as global logger
######################################################## SPLITTED SUBFUUNC
# variables (config) and functions from
#from mysconfig import read_myservice_config, config # config is not global
import mysconfig
import mysinfinite
import mysscreen
import myscommands

g.logger.debug( "RANDOM_STR= "+ str(g.RANDOM_STR)  )

########### NICE END of program ## with save
#
#  CTRL C
#
####################################
def signal_handler(signal, frame):
    mysinfinite.update_table_data()
    print( g.t.move(g.t.height-2,0) + " " )
    #print('You pressed Ctrl+C!')
    mysconfig.save_myservice_config( True ) # overwrite all debug, whatever
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    g.logger.info("[s] connecting %s" % g.ZMQ_REP_PORT )
    socket.connect("tcp://127.0.0.1:%s" % g.ZMQ_REP_PORT)
    try:
        socket.send_json( {'secret':str(g.RANDOM_STR),'message':'kill'} )
        g.logger.info("[s] kill message sent %s; sleep 0.5" % g.ZMQ_REP_PORT )
        time.sleep(0.5)
    except:
        g.logger.error("[s] cannot zmq kill from signalhandler")
    print("exit   %s" % g.ZMQ_REP_PORT )
    g.logger.info("[s] sys.exit(0)  from signal handled")
    sys.exit(0)


###################################
#
#  USR1   save
#
####################################
def signal_handler1(signal, frame):
    #mysinfinite.update_table_data()
    g.logger.info('[s]     USR1 received ...  saving  '+g.MYSECONFIG)
    mysconfig.save_myservice_config( True ) # overwrite all debug, whatever
    g.logger.info('[s]     USR1 received ...  SAVED '+g.MYSECONFIG)



#################################
#
#  USR2   read
#
####################################
def signal_handler2(signal, frame):
    #mysinfinite.update_table_data()
    #print( g.t.move(g.t.height-2,0) + " " )
    g.logger.info('[s]    USR2 received ...  reading '+g.MYSECONFIG)
    g.config=mysconfig.read_myservice_config()

    g.config=mysconfig.read_desc_from_files( g.config ) # desc from 2ndline

    g.logger.info('[s]    USR2 received ...  READ '+g.MYSECONFIG)
    #sys.exit(0)





if __name__=="__main__":
#    print("================ myservice===============")

    ######################################################
    #  MAIN
    ######################################################


    #mysconfig.read_myservice_config()
    #print( g.config )
    #mysinfinite.run_infinite()

    CMDLIST= ['perm','stop','start','onstart','never','enable','disable','restart','undef','enter','status', 'sock','reconfig', 'reload']
    command=""


    if mysscreen.screen_ls_infinite():
        g.logger.info("myscreen2_infinite screen      RUNNING")
    else:
        g.logger.error("myscreen2_infinite screen     NOT RUNNING   !!!")

    aa=[]
    g.logger.debug("checking /etc/rc.local for myscreen2_infinite ")
    try:
        with open("/etc/rc.local","r") as f:
            aa=f.readlines()
    except:
        aa=[]
        print("##########################################\nPLEASE:  CREATE:")
        print("/etc/rc.local")
        print("#!/bin/bash\n\n")
        print('su - '+g.user_name+' -c "screen -dmS myservice_infinite myservice infinite" ')
        print("\n\n\nexit 0")
        print("sudo chmod +x /etc/rc.local\n################################\n")

    aa=[ x for x in aa if x[0]!="#"]
    aa=[ x for x in aa if x.find("myservice_infinite")>0]
    aa=[ x for x in aa if x.find( g.user_name )>0]
    if len(aa)==0:
        g.logger.error('     PLEASE: put into   /etc/rc.local : ')
        g.logger.error('su - '+g.user_name+' -c "screen -dmS myservice_infinite myservice infinite" ')
        #time.sleep(0.4)
        #g.logger.error('su - '+g.user_name+' -c "screen -dmS myservice_infinite myservice infinite" ')
        #time.sleep(0.4)
        #g.logger.error('su - '+g.user_name+' -c "screen -dmS myservice_infinite myservice infinite" ')

    g.logger.debug("checking /etc/rc.local .... CHECKED ")

    #=====================================
    #   READ CONFIG at the very beginning
    #=====================================
    g.logger.debug("reading CONFIG - probably the only once ....... ")
    g.config=mysconfig.read_myservice_config()
    g.config=mysconfig.read_desc_from_files( g.config ) # desc from 2ndline





    #=====================================
    #   READ lock ... if known => INDUCE SAVE
    #                attention - if reconfig=> NO SAVE but READ
    #=====================================
    g.logger.debug( "   LockFile    == "+g.lockfile  )
    if os.access( g.lockfile , os.F_OK):
        pidfile = open( g.lockfile , "r")
        pidfile.seek(0)
        old_pd = pidfile.readline()
        g.logger.debug("   lock content=%s" % old_pd)
        old_pd = int( old_pd.rstrip())
        # Now we check the PID from lock file matches to the current
        # process PID
        if os.path.exists("/proc/%s" % old_pd):
            g.logger.debug("INFINITE ALREADY running as pid %s  (/proc/  check)" % old_pd)
            if g.args.service=="infinite" and mysscreen.screen_ls_infinite():
                g.logger.info("trying to: screeen -x myservice_infinite; Ctrl-a d TO EXIT")
                time.sleep(0.4)
                mysscreen.screen_enter("myservice_infinite")
                g.logger.info("...after screen x myservice_infinite...QUIT")
                if not mysscreen.screen_ls_infinite():
                    g.logger.error("    myscreen2_infinite   NOT RUNNING")
                    time.sleep(0.4)
                    #g.logger.error("    myscreen2_infinite   NOT RUNNING")
                    #time.sleep(0.4)
                    #g.logger.error("    myscreen2_infinite   NOT RUNNING")
                    #time.sleep(0.4)

                print("COMMIT FROM {}".format(mysversion.vdate) )
                sys.exit(1)
                # quit()
            else:
                g.logger.debug("not trying to enter screen -x")
            g.lockfilepid=old_pd
        else:
            g.logger.error("   process not found")
            g.logger.error("   Removing lock file with pid== %s" % old_pd)
            os.remove( g.lockfile )
    if g.lockfilepid==0:
        g.logger.error("   ZERO LockFilePid == " + str(g.lockfilepid) )
    else:
        g.logger.info("   GOOD LockFilePid == " + str(g.lockfilepid) + "   ... sending save signal sigusr1")
        #================================= RECONFIG
        #================================= RECONFIG
        #================================= RECONFIG
        if (g.args.service=='reconfig') or (g.args.service=='reload'):
            g.logger.info("    ... RELOAD CONFIG and exit (sigusr2 send:)")
            g.logger.info("    ... used in manual intervention to "+g.MYSECONFIG)
            os.kill( g.lockfilepid,  signal.SIGUSR2 ) #  READ ...+g.MYSECONFIG
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) #quit()
        os.kill( g.lockfilepid,  signal.SIGUSR1 ) #  SAVE ... overwrites eventually the +g.MYSECONFIG








    #------------------LIST WHEN NO PARAMETERS-------
    if g.args.service is None and g.args.command is None: #
        table=mysinfinite.update_table_data( )            # every second?
        print(  table.table )
        sys.exit(1) #quit()

    # ======================================= PLAIN LIST START
    # ======================================= PLAIN LIST START
    # ======================================= PLAIN LIST START
    # NOT ANYMORE if g.args.service is not None and g.args.command is None: #
    if g.args.service=="list": # I INTRODUCED A KEYWORD !!!!!! UNSYSTEMATIC !!!!
    #if g.args.service is None:
        g.logger.info("list all executable files============")
        print("  *   ... is in "+g.MYSECONFIG)
        print("  E   ... has an atribute enable:")
        print("  +/- ... enabled set to true/false :")
        print("  p   ... is set permanent :")
        print("  a   ... is set AT time :")
        print("  x   ... is set NEVER :")
        #g.logger.info("list all executable files============")
        print("---------------------------------------------")
        #mysconfig.read_myservice_config()
        for i in sorted(mysconfig.get_list_myservice_files() ):
            #logger.debug( "FILES: {}".format(i) )
            mark=""
            iexe=i.split("/")[-1]
            ipath=i.split("/")[0]
            if len(i.split("/"))==1: ipath=""
            g.logger.debug("        i=/{}/  iexe=/{}/ ipth=/{}/".format(i,iexe,ipath))  # i=="TTT/aaa" is not but aaa is


            # somewhere we forgot to define path
            if iexe in g.config.keys() and not "path" in g.config[iexe].keys():
                g.config[iexe]["path"]="" # somewhere we forgot to define "path"
            if iexe in g.config.keys() and (ipath!="" and  ipath==g.config[iexe]["path"]) or (ipath=="" and iexe in g.config.keys()):
                mark+="* "
                if  'enabled' in g.config[iexe]:
                    mark+="E"
                    if g.config[iexe]['enabled']:
                        mark+="+ "
                        if g.config[iexe]['status'].find('perm')==0:
                            mark+="p"
                        elif g.config[iexe]['status'].find('at')==0:
                            mark+="a"
                        elif g.config[iexe]['status'].find('never')==0:
                            mark+="x"
                        elif g.config[iexe]['status'].find('sock')==0:
                            mark+="s"
                        else:
                            mark+=" "
                    else:
                        mark+="-  " # strange "+ "
                else:
                    mark+="    " #no "enabled" attribute in json
            else:
                mark+="      "   #no line in json
            if i in g.config and 'desc' in g.config[i]:
                print("{} {:19s} {}".format(mark,i,g.config[i]['desc'] ) )
            else:
                print("{} {:19s}".format(mark,i ) )

        print("---------------------------------------------")
        g.logger.info("list all executable files end--------")
        print("COMMIT FROM {}".format(mysversion.vdate) )
        sys.exit(1) #quit()
    #========================================    PLAIN LIST END





    ######################################################
    #    deciding if infinite and quit if not
    ######################################################
    if g.args.service is not None and g.args.command is None:
        g.logger.debug( 'service= {}'.format(g.args.service) )
        if g.args.service!="infinite":
            g.logger.error( "awaiting infinite or the second argument missing:")
            g.logger.error( " ... enable, disable, start, stop, restart, perm")
            table=mysinfinite.update_table_data( )            # every second?
            print(  table.table )
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) #quit()
        # infinite command was sent
        #   not screen_ls_infinite(): # screen still not running
        if not mysscreen.screen_ls_infinite(): # screen still not running
            # Here I will run infinite ==========================PIDLOCK
            if os.path.exists("/proc/%s" % g.lockfilepid ):
                g.logger.error("infinity running/ proc %s.  I quit..." %  g.lockfilepid)
                print("COMMIT FROM {}".format(mysversion.vdate) )

                sys.exit(1) # quit()
            g.logger.info("screen myservice_infinite .. NOT running - going on")
            g.logger.info( "sty={}  term={}".format( os.getenv("STY"), os.getenv("TERM") )     )
            term=os.getenv("TERM")
            if term!="screen":
                g.logger.info("SCREEN INFINITE: screen -dmS myservice_infinite  myservice infinite")
                g.logger.info("screen -dmS myservice_infinite  myservice infinite")
                time.sleep(1)
                #g.logger.info("screen -dmS myservice_infinite  myservice infinite")
                #time.sleep(1)
                #g.logger.info("screen -dmS myservice_infinite  myservice infinite")
                #time.sleep(1)
                sp.Popen('nohup screen -dmS myservice_infinite myservice infinite',
shell=True, stdout=None, stderr=None, preexec_fn=os.setpgrp )

                sys.exit(1) # quit()
                g.I_AM_INFINITE=True
            else:
                g.logger.info("  i am in a screen session ... STARTING INFINITE")
                g.I_AM_INFINITE=True

            if g.I_AM_INFINITE:
                ########################
                #   signals catching only in INFINITE, else problems
                ########################
                g.logger.info("===============  SIGNAL HANDLERS INSTALLED======")
                signal.signal(signal.SIGINT,  signal_handler) #  CTRL-C : Legal quit
                signal.signal(signal.SIGUSR1, signal_handler1)#  kick to save config
                signal.signal(signal.SIGUSR2, signal_handler2)#  kick to read config
                mysinfinite.run_infinite()
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) # quit()
        else:
            g.logger.info(" infinite command given; screen myservice_infinite.. running")
            g.I_AM_INFINITE=True
            g.logger.info("===============  SIGNAL HANDLERS INSTALLED 2======")
            signal.signal(signal.SIGINT,  signal_handler) #  CTRL-C : Legal quit
            signal.signal(signal.SIGUSR1, signal_handler1)#  kick to save config
            signal.signal(signal.SIGUSR2, signal_handler2)#  kick to read config
            mysinfinite.run_infinite()
        ### DEAD END HERE ##############
        print("COMMIT FROM {}".format(mysversion.vdate) )

        sys.exi(1) #quit()
    else:
        g.logger.debug('service= {}'.format(g.args.service) )
        if g.args.service=="infinite":
            g.logger.error( "infinite can be only alone as a parameter")
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) #quit()
        g.logger.debug( 'command= {}'.format(g.args.command) )
        command=g.args.command
        # IDENTIFY BAD COMMADS:--------------------
        if not command in CMDLIST and command.find('perm')!=0 and command.find('at')!=0:
            g.logger.error("Bad command "+command)
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) #quit()




    ######################################################
    #    infinite was solved above.....
    #
    #   COMMANDS
    ######################################################

    service=g.args.service
    if service.find("/")>=0:
        g.logger.error("path in service name is not allowed anynmore")
        print("COMMIT FROM {}".format(mysversion.vdate) )

        sys.exit(1) # quit()
    g.logger.debug("Main part... {}:{}".format( service,command ) )
    g.logger.debug("Main part... {}:{}".format( "i must find if service qualifies!!!","") )







    ######################################################
    #  REAL MAIN PART
    ######################################################
    ## EnABLE==DISPLAY === also introduce the service to .CONFIG.JSON
    if command=='enable':
        myscommands.myservice_enable( service )
    if command=='disable':
        myscommands.myservice_disable( service )



    # PERM=green; socket=yellow; stop=gray
    # STATUS ..... ==============
    #infinite will make the job PERM
    if command.find('perm')==0:
        if len( command )>4:
            st=command[4:]
            g.logger.info( st )
            if (st[-1]!='s')and(st[-1]!='m')and([-1]!='h')and([-1]!='d'):
                g.logger.error("perm_unreadable interval ")
                print("COMMIT FROM {}".format(mysversion.vdate) )

                sys.exit(1) # quit()
        myscommands.myservice_set_status( service , command )

            #infinite will make the job AAT
    if command.find('at')==0:
        if command[4]!=":" or len(command)!=7:
            g.logger.error("'at' command form: atHH:MM ... e.g. at00:01")
            print("COMMIT FROM {}".format(mysversion.vdate) )

            sys.exit(1) #quit()
        myscommands.myservice_set_status( service , command )


    ##--------------
    #  special commands - change status onnly
    ##--------------
    if command=='never':
        myscommands.myservice_set_status( service ,'never')
    if command=='undef':
        myscommands.myservice_set_status( service ,'undef')
    if command=='status' or command=='sts' :
        myscommands.myservice_get_status( service )
    if command=='sock':
        myscommands.myservice_set_status( service ,'sock')



    # ACTION COMMANDS    ==============  stop forever
    if command=='stop':
        myscommands.myservice_set_status( service ,'undef')#?
        mysscreen.screen_kill( service )
    # ACTION COMMANDS    ==============  leave the status as is
    if command=='restart':
        myscommands.myservice_restart( service )
    if command=='start':
        myscommands.myservice_start( service  )
    if command=='enter':
        mysscreen.screen_enter( service  )



    g.logger.info("--------------leaving myservice ----------------".format( ) )


    #pidfile.close # no, i do immediately
print("COMMIT FROM {}".format(mysversion.vdate) )
