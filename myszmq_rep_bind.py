#!/usr/bin/env python3
################################
#
#   REQ-REP     (REP listens, REQ is client)
#        not PUB-SUB  (PUB listens, SUB receives)
#
# this is a fragile part, running in background
#  if there is a crash, may remain dead unseen
#
################################
import zmq
import time
import mysglobal as g #  args, logger @every module
import myscommands
import os # i need extra log append user

import sys

def try_bound_port( sock, port ):
    res=True
    try:
        g.logger.info("trying to bind port {}".format(port) )
        #sock.bind("tcp://127.0.0.1:{}".format(port) ) # LOCAL
        sock.bind("tcp://0.0.0.0:{}".format(port) )    # PUBLIC
        res=True
    except:
        g.logger.error("! socket {} is already bound".format(port) )
        res=False
    return res




def resend_json(port2 , JS ):
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    socket.connect("tcp://127.0.0.1:%s" % port2)
    try:
        socket.send_json( JS )  # <-- error occurs here
        #print("          [ok]")  # this will get printed
    except:
        print("         !  error sending json to port+1  ",port2)

    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)
    socks = dict(poller.poll(1 * 500))
    if socket in socks:
        try:
            msg_json = socket.recv_json()
        except IOError:
            if args.debug:print(i,request,"  !Could not connect to machine")
#        else:
#        if args.debug:print(i,request,"    !Machine did not respond ... ")
    return


def receive_jsons( port ):
    # ZeroMQ Context
    context = zmq.Context()
    # Define the socket using the "Context"
    sock = context.socket(zmq.REP)
    res=False
    port2=port
    while not res:
        res=try_bound_port( sock,port2 )
        if not res:port2=port2+1
        if port2>port+5:
            g.logger.error("NO MORE TRIALS WITH PORT, "+str(5)+" is enough")
            sys.exit(1) #quit()
    g.logger.info("ZMQ REP:  port {} is bound".format(port2) )
    g.ZMQ_REP_PORT=port2  # IS THAT ENOUGH ???? FOR ALL MS2 TO KNOW?
    # Run a simple "Echo" server
    i=0
    while True:
        i=i+1
        #print(i,"receiving")
        message = sock.recv_json()
        #
        # MESSAGE RECEIVED ..... GOOD TO SEND TO port+1
        #
        resend_json( port2+1 , message )
        g.logger.debug("[R] RECV: ")
        g.logger.debug(message)

        # FORM BOTTOMLINE-----------------
        #   check for kill message with a secret
        TXT=""
        PARAMETER=""
        if 'time' in message.keys():
            TXT=TXT+"["+message['time']+"] "
        if 'from' in message.keys():
            TXT=TXT+"(From:"+message['from']+") "
            with open( os.path.expanduser("~/myservice_nczmq_"+message['from']+".log"),"a" ) as f:
                f.write( str(message) +"\n" )
        if 'run' in message.keys() and len(message['run'])>0:
            TXT=TXT+"/"+message['run']+"/ "
            with open( os.path.expanduser("~/myservice_nczmq_"+message['run']+".log"),"a" ) as f:
                f.write( str(message) +"\n" )
        if 'parameter' in message.keys() and len(message['parameter'])>0:
            PARAMETER=message['parameter']


        if 'message' in message.keys():
            TXT=TXT+message['message']
            if 'secret' in message.keys():
                g.logger.info("[R] MY  secret:"+str(g.RANDOM_STR))
                g.logger.info("[R] GOT secret:"+message['secret'])
                if message['message']=='kill' and message['secret']==str(g.RANDOM_STR):
                    sys.exit(1) #quit()
        g.logger.debug("[R] BTOM: "+TXT)
        g.BOTTOMLINE_TEXT=TXT
        #======= send back =================
        sock.send_json( {} )
        #print( message )
        #time.sleep(0.1)
        # HERE I SHOULD FIND A WAY HOWTO RUN SMOOTHLY WITHOUT CRASH
        g.logger.debug("[R] TRY RUN: ")

        if 'run' in message.keys() and len(message['run'])>1:
            service=message['run']  # a bit confusing: JSON  run == service name
            g.logger.debug("[R] TRY RUN: " + service)
            #if service in g.config.keys() and g.config[service]['status']=="sock":
            if service in g.config.keys():
                g.logger.debug("[R] TRY myservice_start: " + service)
                if g.config[service]['status']=="sock":
                    g.logger.debug("[R] is sock ...TRY myservice_start: " + service)
                    myscommands.myservice_start( service , dontrun='perm' , param=PARAMETER )
                else:
                    g.logger.debug("[R] NOT sock status!! : " + service)

            else:
                g.logger.debug("[R] NO SERVICE: " + service)




if __name__=="__main__":
    print("ZMQ REC receive  TEST on port 5678")
    receive_jsons( 5678 )
