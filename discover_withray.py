#!/usr/bin/env python3

import fire
import subprocess as sp
import socket
import os

import ray
from influxdb import InfluxDBClient

#----------------------------------------

def func1( n=5 ):
    print("D... func1")
    return n

def test_func1():
    print("D... test func1")
    assert func1(1)==1


def write_influx(IP="147.231.102.63",MYIP="147.231.102.63"):
    print("D... write infl")
    client = InfluxDBClient(IP, 8086, 'ojr', 'ojrojr', 'test')
    json_body = [ {"measurement":MYIP} ]
    print( json_body )
    #json_body[0]["measurement"]=IP
    json_body[0]["fields"]={"value":1}
    print( json_body )
    client.write_points(json_body)

#--------------------------------------------
ray.init(num_cpus=16,memory=52428800,object_store_memory=78643200)

@ray.remote
def ping_port( IPT, port):
    print("  "+IPT+":"+str(port), end=" ")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(1)
    result = sock.connect_ex((IPT, port))
#    if result == 0:
#        print(" Open")
#    else:
#        print(" ----")
    sock.close()
    return IPT,result



def discover_port(port=8086):
    print("D... looking for port",port)
    #hostname = socket.gethostname()
    #IPAddr = socket.gethostbyname(hostname)
    #print("Your Name is:\t" + hostname)
    #print("From hosts:\t" + IPAddr)
    #print("From fqdn :\t", socket.getfqdn() )
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    IP=s.getsockname()[0]
    print("i... IP From 8.8.8.8:\t",s.getsockname()[0])
    s.close()
    IP3=".".join( IP.split(".")[:3] )
    print("i... Scan ",IP3 +".*")

    result_ids = []

    for i in range(1,254):
    #for i in range(60,70):
        IPT=IP3+"."+str(i)
        result_ids.append( ping_port.remote(IPT,port) )
        #i,r=ping_port(IPT,port)
        #print(i,r)
    results = ray.get(result_ids)  # [0, 1, 2, 3]
    okresults=[]
    print( "i... total results = ",len(results) )
    for i in results:
        if i[1]==0: okresults.append( i[0] )

    with open( os.path.expanduser("~/.myservice_discover8086"),"w") as f:
        for i in okresults:
            f.write( i +"\n")
            print(i)
            write_influx(i,IP)
#============================================


from threading import Thread
def thrfun(i,di):
    print("thread ",i)
    di[i]=i
    return i

def threadret():
    ret={}
    t=[]
    for i in range(10):
        t.append( Thread(target=thrfun,args=(i,ret)) )
        t[-1].start()
    for i in t:
        i.join()
    print(ret)


if __name__=="__main__":
    print("D.. from main:")
    fire.Fire( threadret )
#    fire.Fire( write_influx )
#    fire.Fire( func1 )
#    fire.Fire( discover_port )
