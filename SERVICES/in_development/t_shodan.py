#!/usr/bin/python3
from shodan import Shodan
import os
import json
import sys
print("========= use with ip address as parameter =================")
with open( os.path.expanduser("~/.shodan.apikey") ) as f:
    apikey=f.read().strip()
print( "/{}/".format(apikey)     )
#quit()
api = Shodan( apikey )

# Lookup an IP
print("INFO")
ip=sys.argv[1]
print( "/{}/".format(ip ) )
#ipinfo = api.host( ip , history=True)
ipinfo = api.host( ip )
#print(ipinfo)
print( json.dumps( ipinfo ,sort_keys=True, indent=4 ) )


quit()

print("BANNER")
# Search for websites that have been "hacked"
for banner in api.search_cursor('http.title:"hacked by"'):
    print(banner)

    
print("ICS ......")    
# Get the total number of industrial control systems services on the Internet
ics_services = api.count('tag:ics')
print('Industrial Control Systems: {}'.format(ics_services['total']))
