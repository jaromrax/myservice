#!/usr/bin/python3
import notify2
#sudo aptitude install python3-notify2

import time
#from multiprocessing import Queue
from queue import LifoQueue, Queue, Empty, Full


class NotifyClass(object):

    def __init__(self, dest, message):
        self.dest = dest
        self.message = message


    def audio(self):
        print("@"+self.audio.__name__, end='')
        return self.message

    def dbus(self):
        print("@"+self.dbus.__name__+' ', end='')
        notify2.init("--Title--")
        #notice = notify2.Notification(self.dest, self.message)
        notice = notify2.Notification( self.message )
        notice.show()
        time.sleep(2)
        return self.message

    def hat(self):
        print("@"+self.hat.__name__+'  ', end='')
        return self.message


    def web(self):
        print("@"+self.web.__name__+'  ', end='')
        return self.message

    def term(self):
        print("@"+self.term.__name__+' ', end='')
        return self.message


    def do( self ):
        return getattr(NotifyClass, self.dest)(self)


q = Queue(6)

q.put( NotifyClass('audio','Using google talk') )
q.put( NotifyClass('dbus' ,'notify-send way - works with gtk') )
q.put( NotifyClass('hat'  ,'Init sense hat and display') )
q.put( NotifyClass('web'  ,'connect to webpy.py') )
q.put( NotifyClass('term' ,'Terminal printout -either xterm or wall -just print') )
#for i in range(5):
#    if q.full(): break
###############
# MAIN PART- accept and queue messages
#
###############
while not q.empty():
    res=q.get().do()
    print("   ", res)

quit()




def sendmessage(title, message):
    notify2.init("Test")
    notice = notify2.Notification(title, message)
    notice.show()
    return

sendmessage("Test", "ahoj")
#########################
# TASK:
#   local and ip applications need to display stuff. messages.
#   myservice could work as ip port for receiving messages
#      and everything else is local...
#  PLAN:
#  queue external - via -
# * notify
# * sense-hat - small num, rolling number
# * audio - mp3 or voice
# * webpage
#https://pymotw.com/2/multiprocessing/communication.html
#http://askubuntu.com/questions/108764/how-do-i-send-text-messages-to-the-notification-bubbles
#====== first attempt  d-bus:
#
#https://askubuntu.com/questions/38733/how-to-read-dbus-monitor-output/142888
############################
