#!/usr/bin/python3
#######################
#  WATCH ANY SYSLOG FILE
#  NOW
#    -   syslog for PORTS
#    -   auth   for sshd lines
# sudo iptables -I INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT 22: "
#
#######################
import logging
from logzero import setup_logger,LogFormatter,colors
import argparse
import os,sys

import subprocess as s
import datetime
import time

import re # auth


import inotify  # rotation of logs need a watch
import time
import traceback
import threading
import inotify.adapters

ALARM_SENTENCE="ACCESS ATTEMPT"

#https://stackoverflow.com/questions/3290292/read-from-a-log-file-as-its-being-written-using-python
#http://www.dabeaz.com/generators/Generators.pdf

#10203  sudo iptables -D INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25006: "
#10206  sudo iptables -D INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25000: "
#-------------------------  deleted and again, correctly..........
#10206  sudo iptables -I INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT 22: "
#            iptables -I INPUT -p tcp --dport 8888 --syn -j LOG --log-prefix "ACCESS ATTEMPT 8888: "
#10208  sudo iptables -I INPUT -p tcp --dport 25000 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25000: "
#10209  sudo iptables -I INPUT -p tcp --dport 25006 --syn -j LOG --log-prefix "ACCESS ATTEMPT 25006: "

####################################
# PARSER ARG
######################################
parser=argparse.ArgumentParser(description="""
------------------------------------------------------------------
 A snippet to watch events defined in iptables
------------------------------------------------------------------
iptables -D INPUT -p tcp --dport 22 --syn -j LOG --log-prefix "ACCESS ATTEMPT SSH"
   ...  defines the log sentence ....
iptables -L INPUT   ...  will list the things
""",usage="""
""",
 formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument('-d','--debug', action='store_true' , help='')
parser.add_argument('varlogfile', action="store", nargs="+") # nargs='+'
args=parser.parse_args()

###########################################
# LOGGING   - after AGR PARSE
########################################
log_format = '%(color)s%(levelname)1.1s... %(asctime)s%(end_color)s %(message)s'  # i...  format
LogFormatter.DEFAULT_COLORS[10] = colors.Fore.YELLOW ## debug level=10. default Cyan...
loglevel=1 if args.debug==1 else 11  # all info, but not debug
formatter = LogFormatter(fmt=log_format,datefmt='%Y-%m-%d %H:%M:%S')
#logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename(sys.argv[0]) )[0]+'.log'
logfile=os.path.splitext( os.path.expanduser("~/")+os.path.basename( args.varlogfile[0] ) )[0]+'.log'
logger = setup_logger( name="main",logfile=logfile, level=loglevel,formatter=formatter )#to 1-50


######################################
#
#    isend-notify  stuff
#
#####################################

def set_environment():
    # i want unity or xfce4 - on p34
    CMD="pgrep -u "+os.environ['USER']+" xfce4|unity-panel"
    pid=s.check_output( CMD.split() ).split()[0].decode("utf8").rstrip()
    logger.debug("I have PID" + str( pid) )
    CMD="grep -z DBUS_SESSION_BUS_ADDRESS /proc/"+pid+"/environ"
    dsba=s.check_output( CMD.split()  ).decode("utf8").rstrip()
    dsba2=dsba.split("DBUS_SESSION_BUS_ADDRESS=")[1]
    logger.debug("I have DSBA "+ str( dsba) )
    logger.debug("I have DSBA2 " + str(dsba2) )
    return dsba2
#mydsba=set_environment()


import notify2
import time
def note(mess, coll=""):
    notify2.init("--Title--")
    notice = notify2.Notification( mess )
    notice.show()
    time.sleep(2)

def note_OLDUBUNTU( mess, col="" ):
    global mysdba
    ICOPA="/usr/share/icons/gnome/32x32/status/"
    ICOPA="/usr/share/icons/breeze/status/64/"
    CMD="notify-send -t 1 -i "+ICOPA
    if col=="green":
        CMD=CMD+"security-high.svg"
    elif col=="red":
        CMD=CMD+"security-low.svg"
    elif col=="yellow":
        CMD=CMD+"security-medium.svg"
    else:
        CMD=CMD+"dialog-question.vg"

    CMDL=CMD.split()
    CMDL.append(mess)
    s.call( CMDL , env={"DBUS_SESSION_BUS_ADDRESS":mydsba} )










########################################
#
#    FOLLOW .......  SAME FOR ALL FILES
#
########################################
def follow(syslog_file):
    syslog_file.seek(0,2) # Go to the end of the file
    while True:
        line = syslog_file.readline()
        if not line:
            time.sleep(0.2) # Sleep briefly
            continue
        yield line


##################################################################
def process(line, history=False):
  if history:
    logger.debug( '=' + line.strip('\n'))
  else:
    logger.debug( '>' + line.strip('\n'))
##################################################################
def follow2( syslog_file ):
    from_beginning = False
    notifier = inotify.adapters.Inotify()
    while True:
      try:
        #------------------------- check
        if not os.path.exists(syslog_file):
          logger.error( 'syslog_file does not exist')
          time.sleep(1)
          continue
        logger.error( 'opening and starting to watch '+ syslog_file)
        #------------------------- open
        file = open(syslog_file, 'r')
        if from_beginning:
          for line in file.readlines():
            process(line, history=True)
            yield line
        else:
          file.seek(0,2)
          from_beginning = True
        #------------------------- watch
        notifier.add_watch(syslog_file)
        try:
          for event in notifier.event_gen():
            if event is not None:
              (header, type_names, watch_path, filename) = event
              if set(type_names) & set(['IN_MOVE_SELF']): # moved
                logger.error( 'syslog_file moved')
                notifier.remove_watch(syslog_file)
                file.close()
                time.sleep(1)
                break
              elif set(type_names) & set(['IN_MODIFY']): # modified
                for line in file.readlines():
                  process(line, history=False)
                  yield line
        except (KeyboardInterrupt, SystemExit):
          raise
        except:
          notifier.remove_watch(syslog_file)
          file.close()
          time.sleep(1)
        #-------------------------
      except (KeyboardInterrupt, SystemExit):
        break
      except inotify.calls.InotifyError:
        time.sleep(1)
      except IOError:
        time.sleep(1)
      except:
        traceback.print_exc()
        time.sleep(1)

##################################################################





#####################################
#
#   REAL START =====================
##
####################################
note( "{}".format( "watch started" ) , "green" )
#logger.info("============:  "+sys.argv[0]+" STARTED")
logger.info("============:  "+args.varlogfile[0]+" STARTED")


#################################### SYSLOG -  OLDSTYLE
#mylogfile = open( args.varlogfile[0] )
#loglines = follow(mylogfile)
##sshlogfile = open("/var/log/auth.log")
##sshloglines = follow(sshlogfile)

loglines=follow2( args.varlogfile[0]  )

if args.varlogfile[0]=="/var/log/syslog":
    logger.info("SYSLOG: ... watching predefined ports")


    for i in loglines:
        if  (ALARM_SENTENCE in i):
            src=re.search( r'SRC=([\.\d]+)',  i)
            dst=re.search( r'DST=([\.\d]+)',  i)
            spt=re.search( r'SPT=([\.\d]+)',  i)
            dpt=re.search( r'DPT=([\.\d]+)',  i)
            if src==None:
                print("Failure to find  SRC  IP")
                logger.error( i )
            #else:
            #    message="{:12s} :{:5s} -> {:12s} :{:5s}".format( src.group(1),spt.group(1),dst.group(1),dpt.group(1) )
            #    logger.info( message )

            if src.group(1)=="127.0.0.1":
                logger.debug(" ........... no action for 127.0.0.1")
            else:
                logger.info(" ........... ALARM ......................")
                print(i)
                #logger.info( i )
                message="{:12s} :{:5s} -> {:12s} :{:5s}".format( src.group(1),spt.group(1),dst.group(1),dpt.group(1) )
                logger.info( "SYSLOG:"+message )
                note( "{}".format( message ) , "red" )

    quit()




#################################### AUTH
elif args.varlogfile[0]=="/var/log/auth.log":
    logger.info("AUTH  : ... watching sshd events ")


    #====================== sshd============================
    # Accepted publickey for ojr from 127.0.0.1
    # Received disconnect from 127.0.0.1
    # Disconnected from 127.0.0.1 port 45244
    #

    for i in loglines:
        if  (" sshd[" in i):
            txt=i.split(' sshd[')[-1]
            src=re.search( r'([\d]+\.[\d]+\.[\d]+\.[\d]+)',  i)
            if (src==None) or (i.find("Received disconnect")>0):
                print("no IP on the line OR rec.discon.\n",i)
                #logger.error( i )
            else:
                print( src.group(1)  )
                print(i)
                print(" ........... ALARM")
                note( "{}".format( i ) , "red" )
                logger.info( "AUTH  :"+ i )
    quit()



else:
    logger.info("NOTHING ... watching nothing:"+" ".join(args.varlogfile) )
