#!/usr/bin/python3
#############################
#  my intention:
#    -  create  ASCII
#    -  create picture
#    -  keep the table
#
"""
reads n42 file with NuPhyPy
-------------------------
creates
  - asci  asc1
  - tab
  - zoomed jpg
  - fullrange png
  - root


 The script is called from ~/Dropbox/JupyterScripts/dropboxrun
"""



import argparse
parser = argparse.ArgumentParser(description='.')
#parser.add_argument('--', '-b', action="store", help='Long and short together')
parser.add_argument('n42file', action="store")

args = parser.parse_args()

NAME=args.n42file

# no XDISPLAY
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
#import os.path
import os

import math #log10

import NuPhyPy.Spectra.n42 as n42
x,ss,deadt,start,duration,livetime,CPS,coef0,coef1=n42.read( NAME  )
#print(x,ss,deadt)




CONV_N42_LOG=os.path.expanduser("~/n42toasci.log")



    

######################################## PREPARE TAB
BNAME=os.path.basename(NAME)
with open(CONV_N42_LOG,"a") as fx:
    fx.write( BNAME+"  =========================================\n" )

### NOT USED OLD::::
TABUL="<tr><td>"+BNAME+"</td>\n"+\
"<td>"+start.strftime("%Y-%m-%d  %H:%M:%S")+"</td>\n"+\
"<td>"+str(duration.total_seconds())+" s </td>\n"+\
"<td>"+" {:.2f}".format(deadt*100)+" %</td>\n</tr>\n"

TABUL="|"+BNAME+"|"+start.strftime("%Y-%m-%d  %H:%M:%S")+\
    "|"+str(duration.total_seconds())+" s"+"|"+" {:.2f}".format(deadt*100)+" % |"

### OK :::

BNAMEnoext=os.path.splitext( BNAME )[0]
BNAMEroot=os.path.splitext( BNAME )[0]+".root"

TABUL="|"+start.strftime("%Y-%m-%d  %H:%M:%S")+\
    "|  " + "<a href=\""+BNAMEroot+"\"> **"+BNAME+"** </a> "+\
    "|"+ str(duration.total_seconds())+" s"+\
    "|"+ str(livetime)+" s"+\
    "|"+" {:.2f}".format(deadt*100)+" % "+\
    "| {:.2f}".format(CPS)+" cps |"

###  NEW  _ NICER  ########################################
TABUL="|"+start.strftime("%Y-%m-%d  %H:%M:%S")+\
    " |  "+BNAME+ \
    " | "+ str(duration.total_seconds())+" s"+\
    " | "+ str(livetime)+" s"+\
    " | "+" {:.2f}".format(deadt*100)+" % "+\
    " | {:.2f}".format(CPS)+" cps |"

## WRITE TAB #########################
OUTFILE=os.path.splitext( NAME )[0]+'.tab'
print('i... writing ',OUTFILE )
with open( OUTFILE, 'w' ) as f:
    f.write( TABUL  )
with open(CONV_N42_LOG,"a") as fx:
    fx.write( TABUL + "\n" )

##  WRITE ASCI ################
# i wonder, ss should be numpy, enumerate shouldnt work...
OUTFILE=os.path.splitext( NAME )[0]+'.asc1'
print('i... writing ',OUTFILE )
with open( OUTFILE , 'w' ) as f:
    for i,val in enumerate( ss ):
        f.write( str( int(val) )+'\n' )
        #f.write( str(i)+' '+str( int(val) )+'\n' )
with open(CONV_N42_LOG,"a") as fx:
    fx.write( OUTFILE + "\n" )

######################## with ROOT create TH1F
import ROOT
#f=ROOT.TFile( BNAMEroot, "recreate" )

BNAMEroot=os.path.splitext( NAME )[0]+'.root'
BNAMEnoext="h_"+BNAMEnoext

f=ROOT.TFile( BNAMEroot, "recreate" )
print("i... ROOT file=", BNAMEroot,"  with ",BNAMEnoext )

with open(CONV_N42_LOG,"a") as fx:
    fx.write( BNAMEroot + "\n" )

h1=ROOT.TH1F(BNAMEnoext,"N42 file converted", ss.shape[0],0,ss.shape[0] )
entries=0
for i in range( ss.shape[0] ):
    entries=entries+ss[i]
    h1.SetBinContent(i,ss[i])
h1.SetEntries( entries )
n1=ROOT.TNamed("start",   "{}".format(start.strftime("%Y-%m-%d  %H:%M:%S") )  )
n2=ROOT.TNamed("duration","{}".format(duration.total_seconds()  ) )
n3=ROOT.TNamed("DT",      "{:.2f} %".format(deadt*100) )
n4=ROOT.TNamed("livetime","{:.2f}".format( livetime) )
n5=ROOT.TNamed("calib0",     "{:}".format( coef0 ) )
n6=ROOT.TNamed("calib1",     "{:}".format( coef1 ) )
n7=ROOT.TNamed("CPS",     "{:.2f}".format( CPS ) )
h1.GetListOfFunctions().Add(n1);
h1.GetListOfFunctions().Add(n2);
h1.GetListOfFunctions().Add(n3);
h1.GetListOfFunctions().Add(n4);
h1.GetListOfFunctions().Add(n5);
h1.GetListOfFunctions().Add(n6);
h1.GetListOfFunctions().Add(n7);
h1.Write()
f.Close()

CPSL=math.log10(CPS)
col='black'
if (CPSL>1.5):col='black'
if (CPSL>2.0):col='navy'
if (CPSL>2.5):col='green'
if (CPSL>3.0):col='teal'
if (CPSL>3.0):col='blue'
if (CPSL>3.2):col='goldenrod'
if (CPSL>3.4):col='orange'
if (CPSL>3.6):col='coral'
if (CPSL>3.8):col='red'
if (CPSL>4.0):col='orchid'
######################## PLOT 2k RANGE 
f,ax=plt.subplots(  figsize=(10,3)  )
ax.plot( x,ss , c=col, lw=1)
ax.semilogy()
ax.set_xlim(0,2000)
ax.grid(True)
ax.set_title( NAME+'    '+start.strftime("%Y-%m-%d %H:%M:%S") )
#f.show()
OUTFILE=os.path.splitext( NAME )[0]+'.jpg' 
print('i... writing ',OUTFILE )
f.savefig( OUTFILE )
###########################   PLOT ALL RANGE png
f,ax=plt.subplots( figsize=(10,3)  )
ax.plot( x,ss , c=col, lw=1)
ax.semilogy()
#ax.set_xlim(0,2000)
ax.grid(True)
ax.set_title( NAME+'    '+start.strftime("%Y-%m-%d %H:%M:%S") )
#f.show()
OUTFILE=os.path.splitext( NAME )[0]+'.png' 
print('i... writing ',OUTFILE )
f.savefig( OUTFILE )

with open(CONV_N42_LOG,"a") as fx:
    fx.write( OUTFILE + "\n" )
