#!/usr/bin/python3

################################################
#   the GOAL is to spider over all directories under  -d
#--------------------------------------------------------------
# SECTIONS:
#      PICS:    find all jpg (optimize?), png
#               IF pic younger => create allpictures.html ipynb
#
#      N42:     find all n42
#               IF tab file is missing or n42 younger => process
#                                n42toasci.py
#               IF n42 younger
#                    for all *.n42 and *.tab : create alln42.html
#
#      RUNxxx:  good to create zip with files (proton,deuteron,...) insdide RUN
#
#
#############

import glob
import subprocess
import nbformat as nbf
import os
import datetime      
import argparse

import datetime
import time    # convert seconds to H M S 
#import subprocess 



DEBUG=True

# DEPENDENCIES ==========>  BETTER WAY IS NEEDED ~!!!
CONV_N42=os.path.expanduser("~/JupyterScripts/n42toasci.py")

WIDTH=360
pics=[]    
n42s=[]

parser=argparse.ArgumentParser(description="""
 ... I convertt n42 to jpg,root,asc1, tab .... if tab is missing or older/n42 => I CONVERT
""")

# MANDATORY  -d path
parser.add_argument('-d','--diveinto', default="", help='the path must be set', required=True)
#
# show more  comparisons - in detail the uusefull comparisons
parser.add_argument('-s','--showmore', action='store_true', default=False, help='show', required=False)
#
# PIC ONLY    N42 ONLY   
parser.add_argument('-n','--n42only', action='store_true', default=False, help='only', required=False)
parser.add_argument('-p','--piconly', action='store_true', default=False, help='only ', required=False)
#
# allconvert == dont bother with time comparison...convert all
parser.add_argument('-a','--allconvert', action='store_true',default=False, help='convert all, dont ask for date comparison')
args=parser.parse_args()





def make_html_pictures(dir, filename):  # in specific directory
    #============== print HTML ============
    print("i... making html in",dir)
    with open(dir+"/"+filename+".html","w") as f:
        f.write("<html>\n<style>\n.imgContainer{\n float:left;\n}</style>\n<body>\n")
        if len(pics)%2!=0:pics.append("")
        print("D... ",sorted(pics[1::2]),sorted(pics[::2]))
        for p,p2 in zip(sorted(pics[1::2]),sorted(pics[::2])):
            
            nodir=os.path.basename(p)
            nodir2=os.path.basename(p2)
            print("i... ",nodir,nodir2)
            htm='<a href="'+nodir+'">\n <img src="'+nodir+'" width="'+str(WIDTH)+'" align="left" hspace="20"></img>\n</a>\n'
            htm2='<a href="'+nodir2+'">\n <img src="'+nodir2+'" width="'+str(WIDTH)+'" align="left" hspace="20"></img>\n</a>\n'
            f.write( '<div>\n'+htm+htm2+"\n</div>" )
        f.write("\n</body>\n</html>\n")





        
#====================================================
#
#    1. organizes the columns, hrefs,  colors...
#    2. newly - moves jpg,png,asc1,root to subdir
#
#=====================================================
def make_html_n42(dir, filename):  # in specific directory

    garbish = '/auto/'  # must have two //
    if not os.path.exists(dir+garbish):
        os.makedirs(dir+garbish)

    print("i... making html in",dir," and a new directory ",dir+garbish)
        
    with open(dir+"/"+filename+".html","w") as f:
        f.write("<html>\n<style>\n.imgContainer{\n float:left;\n}</style>\n<body>\n")
        ############################
        tablea="""
        <table style="width=100%">  
<tr>
    <th>start</th>
    <th>name</th> 
    <th>duration</th>
    <th>livetime</th>
    <th>DT</th>
    <th>CPS</th>
    <th>asc</th>
    <th>root</th>
</tr>
"""
        f.write(tablea)
        ###
        for i in n42s:  ###################### parse n42 files
            
            asci0=    os.path.splitext(i)[0]+".asc1"
            print( "=============asci0 ",asci0 )
            rootfile0=os.path.splitext(i)[0]+".root"
            jpgfile0= os.path.splitext(i)[0]+".jpg"
            pngfile0= os.path.splitext(i)[0]+".png"
            # new versions
            #------------------------------- this part helps to MOVE
            asci=     os.path.dirname(asci0)    + garbish+os.path.basename(asci0)
            rootfile= os.path.dirname(rootfile0)+ garbish+os.path.basename(rootfile0)
            jpgfile=  os.path.dirname(jpgfile0) + garbish+os.path.basename(jpgfile0)
            pngfile=  os.path.dirname(pngfile0) + garbish+os.path.basename(pngfile0)

            if os.path.exists(asci0):
                os.rename( asci0, asci)
            if os.path.exists(rootfile0):
                os.rename( rootfile0, rootfile)
            if os.path.exists(jpgfile0):
                os.rename( jpgfile0, jpgfile )
            if os.path.exists(pngfile0):
                os.rename( pngfile0, pngfile )
            #------ good for HTML ------------------------
            asci=      "."+garbish+os.path.basename(asci0)
            rootfile=  "."+garbish+os.path.basename(rootfile0)
            jpgfile=   "."+garbish+os.path.basename(jpgfile0)
            pngfile=   "."+garbish+os.path.basename(pngfile0)

                
            #  take info from TAB ##########
            itab=os.path.splitext(i)[0]+".tab"
            print("i...   n42 ", i , itab)
            tab=[]
            # i have a bug with space in path:  test\ test...cannot open
            with open( itab ) as ft:
                tab=ft.read().rstrip().split("|")
            tab=[ x for x in tab if len(x)>0]
            #print("DEBUG tab = ", tab)
            #### continue writing to f: html
            i=os.path.basename(i)
            f.write("<tr>\n")
            txt='<td>'+tab[0]+'</td>'
            tab[1]=tab[1].replace("*","")  #  ** ** md bf
            txt+='<td><strong>'+tab[1]+'</strong></td>'     # n42file
            ti1=float(  tab[2].split()[0]  )
            ti2=float(  tab[3].split()[0]  )
            #print("DEBUG time float=", ti1)
            #txt+='<td>'+time.strftime('%dd %H:%M:%S', time.gmtime( ti1 ) )+'</td>'    # duration to HMS
            txt+='<td>'+str(datetime.timedelta(seconds= round(ti1)  ))+'</td>'    # duration to HMS
            #txt+='<td>'+time.strftime('%dd %H:%M:%S', time.gmtime( ti2 ) )+'</td>'    # livetime to HMS
            txt+='<td>'+str(datetime.timedelta(seconds= round(ti2)  ))+'</td>'    # duration to HMS
            txt+='<td>'+tab[4]+'</td>'  #%  DT%
            txt+='<td>'+tab[5]+'</td>'  #


            # ------ prepare reference to root ---------------
            ####root='<a href="'+rootfile+'">'+refjpg+'</a>'  #  THIS IS REFERENCE TO ROOT
            # ------ -- reference to asc --------------------  
            asci='<a href="'+asci+'">'+asci+'</a>'+'<a href="'+rootfile+'">  root</a>'
            txt+='<td>'+asci+'</td>'
            # --------  jpg - small      --------------------
            # height NOTWOR refjpg='<img src="'+jpg+'"  width="300" height="50%" />'
            #
            refjpg='<img src="'+jpgfile+'"  width="200" />'
            jpg= '<a href="'+jpgfile+'">'+refjpg+'</a>'  #  THIS IS REFERENCE TO PICTURE

            
            txt+='<td>'+jpg+'</td>'   # APPLY REFERENCE   jpg / root
            f.write( txt+"\n")
            f.write("</tr>\n")
        ###
        f.write("</table>\n")
        ############################
        f.write("\n</body>\n</html>\n")
        






        
def html_to_notebook(dir, filename):
    print("i... converting",filename,".html in",dir)
    nb = nbf.v4.new_notebook()
    fname=dir+"/"+filename+".ipynb"
    text='from IPython.display import HTML\nHTML("'+filename+'.html")'
    #nb['cells'] = [nbf.v4.new_markdown_cell(text)]
    nb['cells'] = [nbf.v4.new_code_cell(text)]
    with open(fname, 'w') as f:
        nbf.write(nb, f)
        
    print('i... compile with jupyter (html to notebook)')
    CMD='jupyter nbconvert --execute --inplace "'+fname+'"'
    p=subprocess.check_call( CMD , shell=True)
    #p=subprocess.call( CMD.split() ) # works when no spaces





    
#########################################
#
#   online spectra viewer
#
###################################
def root_to_notebook(dir, filename):

    garbish = '/auto/'  # must have two //
    if not os.path.exists(dir+garbish):
        os.makedirs(dir+garbish)

    if os.path.exists( dir+filename ):
        os.rename( dir+filename,  dir+garbish+ filename )

        
    print("i... preparing",filename,".ipynb in",dir)

    nb = nbf.v4.new_notebook()
    fname=dir+garbish+"/"+filename+".ipynb"
    #text='from IPython.display import HTML\nHTML("'+filename+'.html")'
    text=('import ROOT\n'
          '%jsroot on\n'
          'h=ROOT.TH1F()\n'
          'inf = ROOT.TFile("'+filename+'.root")\n'
          'for i in inf.GetListOfKeys():\n'
          '    c2 = ROOT.TCanvas()\n'
          '    key=i.GetName()\n'
          '    h=inf.Get(key)\n'
          '    h.Draw()\n'
          '    break\n'
          'c2.Draw()\n'
    )
    
    #nb['cells'] = [nbf.v4.new_markdown_cell(text)]
    nb['cells'] = [nbf.v4.new_code_cell(text)]
    with open(fname, 'w') as f:
        nbf.write(nb, f)
        
    print('i... compile with jupyter   (root to notebook)')
    CMD='jupyter nbconvert --execute --inplace "'+fname+'"'
    p=subprocess.check_call( CMD , shell=True) # trying to save spacesbug 
    #p=subprocess.call( CMD.split() )# works without spaces 

    






    
def timemod(file_name):    
    try:
        mtime = os.path.getmtime(file_name)
    except OSError:
        mtime = 0
    return  mtime





def is_html_younger( html,  flist ):
    # compare [any of list] and html
    #  html= full path+name
    # if html younger => do nothing else remake html
    # latest time of pics ####
    # print( html, flist )
    recent=timemod(flist[0])
    for p in flist:  # find the most recent from flist ....
        ll=timemod(p)
        if ll>recent:
            #rec2=datetime.datetime.fromtimestamp(recent).strftime("%Y/%m/%d  %H:%M:%S")
            #ll22=datetime.datetime.fromtimestamp(ll   ).strftime("%Y/%m/%d  %H:%M:%S")
            #if DEBUG:print("D...     {}..{} is more recent than {}".format( p,ll22,rec2)  )
            recent=ll
    ll2=timemod( html )
    rec2=datetime.datetime.fromtimestamp(recent).strftime("%Y/%m/%d  %H:%M:%S")
    ll22=datetime.datetime.fromtimestamp(ll2   ).strftime("%Y/%m/%d  %H:%M:%S")
    if ll2>recent:
        if args.showmore: print("i...        the whole LIST ",len(flist)," is OLDER  : list=",rec2,"html=",ll22,"     no action")
        #print("-... doing NOTHING")
        print("",end="")
        return True
    else:
        print("i...        someone from the list is younger than HTML : list=",rec2,"html=",ll22,"    GO! GO! GO!")
        return False




################## MAIN #############################
################## MAIN #############################
################## MAIN #############################
if __name__=="__main__":
   
    ##########################################################
    #
    #   ALL DIRS
    #
    ##########################################################
    

    #alldirs=glob.glob('../Mg26/20171115_dp26mg/**/', recursive=True) # ** means all subdirs

    alldirs=glob.glob( args.diveinto+'/**/', recursive=True) # ** means all subdirs
    ###########################################################
    #
    #  this part is able to create  alln42.html and .ipynb
    #
    ###########################################################
    NAME="alln42"   # NAME FOR HTML FILE 
    NAME="0000_n42"   # NAME FOR HTML FILE 
    if DEBUG:print("D... {} : {} directories in total =============================".format( NAME, len(alldirs) ) )
   #############################################
    #    TAB   with links to n42, root, zip, minipicture?
    #    better in plain HTML
    #
    #                        %run -i n42toasci.py 4daysbg.n42
    #            compare with  tab file
    #
    #    could replace jupyfun
    ##########################
    if not args.piconly:
        for dir in sorted(alldirs):
            #   CREATE  GLOBAL  LIST n42s --- key= modification time. Not spe time
            n42s=list( sorted(glob.glob(dir+"*.n42" ),key=os.path.getmtime ))
            if len(n42s)==0:continue
            if DEBUG:print("d... {:3d} n42 files in  {}".format( len(n42s), dir ) )
    
            if len(n42s)>0:
                #if DEBUG:print( "d... ######",dir, len(n42s), NAME,"  ########" )
                for i in n42s:  # compare every .tab  with the n42 - one to one 
                    tab= os.path.splitext( i )[0]+".tab"
                    if is_html_younger( tab, [i] ) and not args.allconvert:
                        print("",end="")
                    else:
                        # : when the directory contains spaces: problem: so ""
                        #     and also shell
                        CMD=CONV_N42+' "'+i+'"'
                        print("\ni... ======== CONVERT n42 to tab,asc1,jpg,root ===============")
                        print(  "i...      === ",CMD)
                        #subprocess.call( CMD.split() ) # works without spaces
                        subprocess.call( CMD, shell=True ) # trying to save spacebug
                        basenamenoex=os.path.splitext( os.path.basename(i) )[0]
                        root_to_notebook(dir, basenamenoex  ) # it will thing of .root and create ipynb

                # I will compare if any TAB file is newer than N42
                #  before it was n42, which didnt always work
                n42tabs=[ os.path.splitext( i )[0]+".tab" for i in n42s ]
                if is_html_younger( dir+"/"+NAME+".html" , n42tabs  ) and not args.allconvert:
                    print("q...     html is newer than  any n42 file ... doing NOTHING")
                    print("",end="")
                else:
                    print("i...  ACTION - creating  html  and a simple ipynb" )
                    make_html_n42(    dir, NAME )
                    html_to_notebook( dir, NAME )
         
    
                        
        if DEBUG:print("D... {}  DONE --------------------------------------".format( NAME ) )
    





        
    ###########################################################
    #
    #  this part is able to create  allpictures.html and .ipynb
    #    MUST GO AFTER  n42 ----  it creates pictures
    ###########################################################
    NAME="allpictures"


    if not args.n42only:
        if DEBUG:print("\nD... {} : {} directories in total =============================".format( NAME, len(alldirs) ) )
        for dir in sorted(alldirs):
            jpgs=list( sorted( glob.glob(dir+"*.[jJ][Pp][Gg]") ,key=os.path.getmtime)  )
            pngs=list( sorted( glob.glob(dir+"*.[Pp][Nn][Gg]") ,key=os.path.getmtime ) )
            pics=jpgs+pngs
            if len(pics)==0:continue
            if DEBUG:print("d... {:3d} PIC files in  {}".format( len(pics), dir ) )
            if len(pics)>0: #---- if dir contains pictures ----
                #print( "######",dir, len(pics),NAME,"  ########" )
                # latest time of pics ####
                if is_html_younger( dir+"/"+NAME+".html" , pics ) and not args.allconvert:
                    #print("q... JPG PNG doing NOTHING")
                    print("",end="")
                else:
                    print("i... ACTION  -  creating  html and  ipynb")
                    make_html_pictures( dir, NAME )
                    html_to_notebook(   dir, NAME )
        
        
        print("i... ", len(alldirs),"directories were globbed")  ##################
        
    
        if DEBUG:print("D... {}  DONE --------------------------------------".format( NAME ) )
