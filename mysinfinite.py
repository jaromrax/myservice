#!/usr/bin/env python3
import mysglobal as g # args,loggerr @every module

import mysscreen
import mysconfig

#from blessings import Terminal  # Color
from colorclass import Color
import datetime
from terminaltables import AsciiTable,SingleTable
import time


import myszmq_rep_bind        # ZMQ REP
from threading import Thread  # thread


import os   # create pidfile


import re  # regex for timedelta_strptime
from datetime import timedelta
# -------  g.config knows everything
def get_permanent_services():
    #global config
    #config=read_myservice_config()
    li=[]
    for service in g.config.keys():
        if g.config[service]['status'].find('perm')==0:
            li.append(service)
    return li

def get_at_services():
    #global config
    #config=read_myservice_config()
    li=[]
    for service in g.config.keys():
        if g.config[service]['status'].find('at')==0:
            li.append(service)
    return li



########################################
#
#   CONVERT   STRING TO TIMEDELTA
#     timedelta_strptime
#
########################################
regexday=re.compile(r'(?P<hours>\d+?):(?P<minutes>\d+?):(?P<seconds>\d+?) (?P<days>\d+?)d$')
regexnoday=re.compile(r'(?P<hours>\d+?):(?P<minutes>\d+?):(?P<seconds>\d+?)$')
def timedelta_strptime(time_str):
    if len(time_str)<7:
        return timedelta(seconds=0)
    # print( "!",time_str )
    if time_str[-1]=="d":
        regex=regexday
    else:
        regex=regexnoday
    #print("!!",regex)
    parts = regex.match(time_str)
    #print("!!!",parts)
    if not parts:
        return
    parts = parts.groupdict()
    #print(parts)
    time_params = {}
    for name, param in parts.items():
        if param:
            time_params[name] = int(param)
            #print("=== ", name, time_params[name])
    #print( "!!!!"+str(time_params ) )
    return timedelta(**time_params)




def translate_shorttime_to_seconds( st ):
    factor=1
    if st[-1]=='s': factor=1
    if st[-1]=='m': factor=60
    if st[-1]=='h': factor=3600
    if st[-1]=='d': factor=24*3600
    g.logger.debug( "mycrash:"+st+ "  "+str(factor) +" "+ st[:-1] )
    try:
        val=int( st[:-1] )
    except:
        val=30 # 30 seconds rather than crash.....
    ret=val*factor
    #g.logger.debug( st+"  to seconds== "+str( ret) )
    return ret








##############################################################
#
# TABLE AND BOTTOMLINE
#
#   colors, times .....
#
###########################################################

def update_table_data(  ):
    global mm
    #get_mmap_file()
    PERM=Color('{autogreen}perm{/autogreen}')   # perm redefined later
    SOCK=Color('{autoyellow}sock{/autoyellow}')
    RUNNING=Color('{autocyan}runs{/autocyan}')
    STOP=Color('{autored}stop{/autored}')
    NEVER=Color('{autoblack}never{/autoblack}')
    STOP_BLACK=Color('{autoblack}stop{/autoblack}')


    #g.config=mysconfig.read_myservice_config()   # DICT


    # clear AGE,SESSION ==============
    files=mysconfig.get_list_myservice_files() # LIST +g.MYSECONFIG
    #g.logger.info( "UpTaDa: files==   "+" ".join(files) )
    # files  not user anymore ???  => check the existence
    for i in g.config.keys():
        g.config[i]['age']=""  # age/or/lastrun
        g.config[i]['session']=""  # I delete session
        if not 'path' in g.config[i]:g.config[i]['path']=""  # I delete session
    # update AGE,SESSION =============
    mysconfig.update_age( )
    #
    li=mysscreen.screen_ls() # update config [age,session]
    g.config=mysconfig.update_config_from_ls( li )
         # [age,session] FROM li (=screen_ls); lastrun fron NOW

    #if not g.DEBUG:mysconfig.save_myservice_config()
    #now=datetime.datetime.now().strftime("%H:%M:%S")
    # ====HEADER ====
    table_data=[  [ 'service',  'mode' , 'AGE', '']   ]

    # ====DISPLAY: ==== if mode== perm OR it runs==(age EXISTs)
    ############ go through the keys ####################
    # but sorted without path look BAD..
    #for iii in sorted( g.config.keys() ):
    #iii2=collections.OrderedDict()
    sortname={}
    for iii in g.config.keys(): # sortname
        sortname[g.config[iii]['path']+"/"+iii]=iii


    #for iii in sorted( g.config.keys() ): # i want sort(path/service)
    for iii2 in  sorted(sortname.keys()) :
        iii=sortname[iii2] #this should be plain servicename
        # COULD BE perm, perm5m, perm1h .....
        desc=g.config[iii]['desc']


        ###  NAME == PATH + iii #####################
        if 'path' in g.config[iii] and g.config[iii]['path']!="":
            name=g.config[iii]['path']+"/"+iii
        else:
            name=iii
        MODE=STOP
        DISP=False # BY DEFAULE
        # FIRST I DECIDE DISPLAY========
        # if config[iii]['status']=='perm':
        #     DISP=True
        # if config[iii]['status']=='undef':
        #     DISP=True
        # if config[iii]['status']=='sock':
        #     DISP=True
        #### BUT NOW I CHANGE ALL:
        DISP=False
        if 'enabled' in g.config[iii].keys():
            if g.config[iii]['enabled']:
                DISP=True
            else:
                DISP=False
                # if disabled but running
                if g.config[iii]['session']!='':
                    DISP=True
                    name=Color('{automagenta}'+name+'{/automagenta}')
        else:
            DISP=True
            # missing 'enabled' in dict
            name=Color('{automagenta}'+name+'{/automagenta}')


        # SECOND I DECIDE MODE=========== (and --NAME-- again )
        #  if undef and runs==> running
        #  if not in files  ==>  myservice_mjpg0,myservice_infinite
        #  files...  backups/backup
        #        ... comes from mysconfig.get_list_myservice_files()
        #            which is config content
        #
        complete_path=[x.split("/")[-1] for x in files]
        if not iii in complete_path:  #     backups/backup
            name=Color('{autoblack}'+name+'{/autoblack}')
        if g.config[iii]['status']=='sock':
            MODE=SOCK
            name=Color('{autoyellow}'+name+'{/autoyellow}')



        # if PERM...    ----STATUS----   COLOR ------
        if g.config[iii]['status'].find('perm')==0:
            PERM=Color('{autogreen}'+g.config[iii]['status']+'{/autogreen}')
            MODE=PERM
            if g.config[iii]['session']!='':
                name=Color('{autogreen}'+name+'{/autogreen}')
        # if AT
        if g.config[iii]['status'].find('at')==0:
            PERM=Color('{autoyellow}'+g.config[iii]['status']+'{/autoyellow}')
            MODE=PERM
            if g.config[iii]['session']!='':
                name=Color('{autogreen}'+name+'{/autogreen}')
        if g.config[iii]['status']=='never':
            MODE=NEVER
            name=Color('{autoblack}'+name+'{/autoblack}')
        # if not perm and running ===> cyan
        if g.config[iii]['status'].find('perm')!=0 and g.config[iii]['session']!='':
            MODE=RUNNING   # CYAN
            name=Color('{autocyan}'+name+'{/autocyan}')
        # if perm and runnig ===> green
        #if g.config[iii]['status'].find('perm')==0 and g.config[iii]['session']!='':
        #    MODE=RUNNING   # CYAN
        #    name=Color('{autocyan}'+name+'{/autocyan}')
        if g.config[iii]['status']=='undef' and g.config[iii]['session']=='':
            MODE=STOP_BLACK



        # LAST I DECIDE COLOR OF AGE=====
        if g.config[iii]['session']!='':
            age=Color('{autogreen}'+g.config[iii]['age']+'{/autogreen}')
            if g.config[iii]['status']=='undef':
                age=Color('{autocyan}'+g.config[iii]['age']+'{/autocyan}')
            if g.config[iii]['status']=='sock':
                age=Color('{autoyellow}'+g.config[iii]['age']+'{/autoyellow}')
        if g.config[iii]['session']=='':
            age=Color('{autored}'+g.config[iii]['age']+'{/autored}')
            #MODE=STOP

        if DISP:# ==================================
            table_data.append( [ name , MODE,  age , desc ] )
    table = SingleTable(table_data)
    #table = AsciiTable(table_data)
    #table.inner_heading_row_border = False
    return table

#############################
#   bottomline is also a decoder of serf
#  probably:
#     serf infrastructure runs as a service
#     event myservice pi4:asadas - will start a service
#     event myservice pi4:asadas:stop
#     event myservice all:asadas
#
############################



def bottomline( i ):
    now=datetime.datetime.now().strftime("%H:%M:%S")
    SEC=g.t.red+"{:02d} ".format(i)
    TIME=g.t.yellow+now+g.t.normal
    #MSG=' This '+g.t.normal+'is '+g.t.underline('a status line!')+g.t.normal+"X"
    #MSG=' This '+g.t.normal+'is '+'a status line!'+g.t.normal+"1234567890"
    MSG=g.BOTTOMLINE_TEXT
    #========MSG=read_mmap_file()  # READ MMAP = should action
    MSGL=list(MSG)[:-10] # remove space for H:M:S
    #logger.info( "is it #?..."+MSGL[0] )
    #if not MSGL[0]=="#":
    #now=datetime.datetime.now().strftime("%H:%M:%S")
        #logger.info( "BL:"+"".join(MSGL[:40]) )
        #MSGL[0]="#"
        #MSG="".join(MSGL)
        #MMAP======write_mmap_file( "#"+now+" "+MSG  )
        # on disk, now display in bottom line
    rema=g.t.width-12 - len(MSG)
    if rema>0:
        MSG=MSG+"-"*rema
    return SEC+TIME+" "+MSG[0:g.t.width-12]







#########################################
#
#   run infinite:  the core
#
#
##########################################





def run_infinite():
    #  config is read @ the very beginning in main
    #mysconfig.read_myservice_config() # FIRST READ OF CONFIG - maybe the last also
    g.logger.debug("[f] running infinite ................")
    g.logger.info( "[f] i am infinite in run_infinite == "+str(g.I_AM_INFINITE) )
    #-PID LOCK -----------------------------------------------
    pidfile = open( g.lockfile , "w")
    g.lockfilepid=os.getpid()
    pidfile.write("%s\n" % g.lockfilepid )
    pidfile.close()
    g.logger.info("[f]    LockFilePid == " + str(g.lockfilepid) )


    #- RUN ZMQ REP ------------------------------
    #myszmq_rep_bind.receive_jsons( 5678 )
    g.thread = Thread(target = myszmq_rep_bind.receive_jsons,
                    args = (g.ZMQ_REP_PORT, ))
    g.thread.start()

    ######################################################
    #   DISPLAY TABLE + bottom line
    ######################################################
    #print(t.clear,endl="")

    while True:
        ###########################################
        # ACTION every 20s



        #################################
        # RUN ALL SERVICES ======== START
        ############################
        #g.logger.info( "[f] i am infinite [sec]== "+str(g.I_AM_INFINITE) )
        liper=get_permanent_services() # perm, perm5m .....
        liat=get_at_services() # ...... at5:55 ????
        li=mysscreen.screen_ls()
        lirun= mysscreen.get_services_from_screen_ls( li )

        mysconfig.update_config_from_ls( li ) # [age,session]; now - age from config

        #g.logger.info( "[f] Running: "+" ".join(lirun) )
        liper=[ i for i in liper+liat if i not in lirun ] # remove all the running
        now_hm=datetime.datetime.now().strftime("%H:%M") # FOR AT
        for service in liper:
            # every service get evaluated the age
            # there is a problem - timedelta doesnt have strptime: NEW:timedelta_strptime
            #
            #
            dt=1
            g.logger.debug( g.config[service]['age'] ) # <<=== crash when reconfig
            #
            delta=timedelta_strptime(  g.config[service]['age']  )
            g.logger.debug( "-->>> "+str(delta) )
            dt=delta.total_seconds()
            # if g.config[service]['age']!="" and g.config[service]['age'][-1]=="d":
            #     tt=datetime.datetime.strptime( g.config[service]['age'], "%H:%M:%S %dd" )
            #     delta=datetime.timedelta(hours=tt.hour,minutes=tt.minute,seconds=tt.second)
            #     dt=delta.total_seconds()
            # if g.config[service]['age']!="" and g.config[service]['age'][-1]!="d":
            #     tt=datetime.datetime.strptime( g.config[service]['age'], "%H:%M:%S" )
            #     delta=datetime.timedelta(hours=tt.hour,minutes=tt.minute,seconds=tt.second)
            #     dt=delta.total_seconds()
            g.logger.debug( service + " age   "+str(dt)+" sec." )


            #---------------------------------- if perm5m, perm1h
            if g.config[service]['status'].find('perm')==0 and len( g.config[service]['status'] )>4:

                interval=g.config[service]['status'][4:]
                #g.logger.debug( "[f] DEBUG AGE == /"+g.config[service]['age'] + "/   interval== "+ interval)
                #g.logger.debug(g.config[service]['age']+"  "+str( dt)  )
                dsec=translate_shorttime_to_seconds( interval )  #  5m  1h
                if dt>dsec:
                    g.logger.info("[f] INF/perm{}/ starting ... {}".format(interval,service))
                    if g.I_AM_INFINITE: mysscreen.screen_new( service ) ###### RUN PERM xHMSD
            #------------------------------- if perm
            elif g.config[service]['status'].find('perm')==0:
                g.logger.info("[f] INF: perm  : starting ... "+service)
                if g.I_AM_INFINITE: mysscreen.screen_new( service ) ###### RUN PERM
            #-------------------------------- if at22:22
            elif g.config[service]['status'].find('at')==0:
                if now_hm==g.config[service]['status'][2:] and dt>60:
                    g.logger.info("[f] AT "+service) #
                    g.logger.info("[f] AT "+g.config[service]['status'][2:]) #
                    if g.I_AM_INFINITE: mysscreen.screen_new( service ) ###### RUN AT xHMSD

        #################################
        # RUN ALL SERVICES ======== END
        ############################


        for k in range(5,0,-1): #   5 seconds between SCREEN LS CALLS
            print( g.t.clear(),end="" )
            table=update_table_data( )  # every second?
            print(  table.table )
            #screen_ls()
            #update_config_from_ls(  )
            #not neces.print( t.move(t.height-1,0)+ " "*t.width+t.move(0,0), end="" )
            print( g.t.move(g.t.height-1,0)+ bottomline(k)+g.t.move(0,0), end="" )
            print( g.t.move(0,0)+"" ) # MUST BE AFTER BOTTOM
            time.sleep( 1 )  # SLEEP 20seconds
        #print("sleep")
        #time.sleep( 0.5 )



if __name__=="__main__":
    print("================ SUBFUNCTION mysinfinite===============")
    run_infinite()
