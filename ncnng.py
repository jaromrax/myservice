#!/usr/bin/env python3


# start a socket to service requests.
# HEY THIS IS EXAMPLE BAD CODE, SO DON'T TRY TO USE IT
# in fact it's so bad it causes a panic in nng right now (2019/02/09):
# see https://github.com/nanomsg/nng/issues/871
#
#  ZMQ is OLD, but still active ... ImageZMQ
#  new generation is too new, butttt...
#
import pynng
import threading

def service_reqs(s):
    while True:
        data = s.recv()
        s.send(b"I've got your response right here, pal!")


threads = []
with pynng.Rep0(listen='tcp://127.0.0.1:12345') as s:
    for _ in range(2):
        t = threading.Thread(target=service_reqs, args=[s], daemon=True)
        t.start()
        threads.append(t)

    for thread in threads:
        thread.join()




######################################################


from pynng import Pair0,Pub0,Sub0, Timeout
import time

print("... create pair (listen)")
s1 = Pair0()
#s1 = Pub0(listen='tcp://127.0.0.1:54321')
s1.listen('tcp://127.0.0.1:54321')

print("... create pair (dial)")
s2 = Pair0()
#s2 = Sub0( dial='tcp://127.0.0.1:54321', recv_timeout=100)
#s2.subscribe(b'')
s2.dial('tcp://127.0.0.1:54321')



time.sleep(0.05)

s1.send(b'Well hello there, pair calling pair')

print("recv in PAIR:",s2.recv())
s1.close()
s2.close()




##################################################3
print("... create pub")
#s1 = Pair0()
s1 = Pub0(listen='tcp://127.0.0.1:54321')
#s1.listen('tcp://127.0.0.1:54321')

print("... create sub")
#s2 = Pair0()
s2 = Sub0( dial='tcp://127.0.0.1:54321', recv_timeout=100)

s2.subscribe(b'')
#s2.dial('tcp://127.0.0.1:54321')

time.sleep(0.05)

s1.send(b'Well hello there, PUB sending to registered SUBs')

print("recv in SUB:",s2.recv())
s1.close()
s2.close()




##################################################3
# statefull protocol; context needed (only here)
print("... create REQ")
#s1 = Pair0()
s1 = Rep0(listen='tcp://127.0.0.1:54321')
#s1.listen('tcp://127.0.0.1:54321')

print("... create sub")
#s2 = Pair0()
s2 = Req0( dial='tcp://127.0.0.1:54321', recv_timeout=100)

s2.subscribe(b'')
#s2.dial('tcp://127.0.0.1:54321')

time.sleep(0.05)

s1.send(b'Well hello there, PUB sending to registered SUBs')

print("recv in SUB:",s2.recv())
s1.close()
s2.close()
