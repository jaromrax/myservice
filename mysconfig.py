#!/usr/bin/env python3
import os
import json
import datetime
import math # round seconds

import glob

import time

import mysglobal as g # args,loggerr @every module


import mysscreen  # want to see if infinite runs...
import myscommands
import signal

#################################
# format AGE display
#################################
def strfdelta(tdelta ):
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    if d["days"]>0:
        return "{:02d}:{:02d}:{:02d} {}d".format(  d["hours"],d["minutes"],d["seconds"], d["days"] )
    else:
        return "{:02d}:{:02d}:{:02d}".format(  d["hours"],d["minutes"],d["seconds"] )






##############
#   CONFIGFILE
# if empty => dumps empty
#
#    save - only INFINITE SHOULD DO. Else it is a mess
##############
def DO_SAVE():
    # CONF=os.path.expanduser(r'~/.myservice/.config.json')
    CONF = g.MYSECONFIG
    g.logger.debug("saving LOCALY ... "+ CONF)
    with open( CONF , 'w') as f:
        json.dump( g.config, f, sort_keys=True, indent=4, separators=(',', ': '))

def save_myservice_config(  force_save=False  ):
    if g.I_AM_INFINITE or force_save: # no discussion
        DO_SAVE()
        if force_save:
            g.logger.debug("FORCED save of "+g.MYSECONFIG)
        return

    g.logger.debug("ONLY INFINITE SHOULD SAVE. A am not")
    if not mysscreen.screen_ls_infinite():
        g.logger.error(" ...  no myservice_infinite screen running")
    if g.lockfilepid>0:
        g.logger.debug(" ... but infinite  is in the terminal somewhere")
        # # 3 # # # # # # # ##
        # i think the 1st SAVE happended in the beginning few seconds ago
        #
        #os.kill( g.lockfilepid, signal.SIGUSR1 )  # save
        #g.logger.info(" ... USR1 - signal to INFINITE to SAVE   ... DONE" )
        #time.sleep(0.3)
    DO_SAVE()
    if g.lockfilepid>0:
        os.kill( g.lockfilepid, signal.SIGUSR2 )  # read
        g.logger.info(" ... USR2 -  signal to INFINITE to READ ... DONE")





def introduce_empty_member( name , TIMESTR ):
    global config
    # I take care also about subdirs... TTT/aaa
    path=""
    if name.find("/")>0:
        path,name=name.split("/")
    g.config[name]={"status":"undef", # never? perm? onstart?
                  "enabled":True,   # This will be display/no display
                  "added":TIMESTR,
                  "lastrun": "",
                  "age": "",
                  "session": "",
                  "desc": "",
                  "path":path,
                  "id":0 }






def update_config_from_ls( li  ):
    """
    analyses the screen -ls output.   "li"
    """
    ### li=screen_ls()
    NOW=datetime.datetime.now()
    # ALL SERVICES SEEN RUNNING HERE
    #    but when not seen. ... i should delete AGE, SESSION
    for i in li:
        # RETRIEVE all data from running screen:
        name=i.split(".myservice_")[1].split()[0]
        session=i.strip().split()[0]
        startup1=i.strip().split("(")[1]
        startup=startup1.split(")")[0]
        # decode time from SCREEN ()
        # ------------ THIS SOMETIMES CRASHES:



#        g.logger.info("potentialCRASH update_config_from_ls  n="+name+" s="+session+" 1="+str(startup1) +" 0="+str(startup) )
        # ============== here it crashes ==============
        # 12/31/23 h12ms pm
        # ============== here it crashes ============== 202104
        stopme = False
        try:
            t=datetime.datetime.strptime( startup, "%m/%d/%Y %I:%M:%S %p")
        except Exception as ex:
            stopme = True
            erm = "X... CRASH time cannotCOULDNT be converted..."+startup+"\n"
            erm = erm+"X... normally I expect ...2030.myservice_seread^I(04/20/2021 02:43:44 PM)^I(Detached)\n\n"
            print(erm)
            with open( os.path.expanduser("~/myservice.errors","a") ) as f:
                f.write(erm)

        # next attempt
        # 04/17/23 09:50:44  = m d %y  ... not %Y
        if stopme:
            try:
                t=datetime.datetime.strptime( startup, "%m/%d/%y %H:%M:%S")
                with open( os.path.expanduser("~/myservice.errors","a") ) as f:
                    f.write(" %m/%d/%y %H:%M:%S  worked well though  \n")
                stopme = False
            except Exception as ex:
                stopme = True
                erm = "X... CRASH time cannotCOULDNT be converted..."+startup+"\n"
                erm = erm+"X... normally I expect ...2030.myservice_seread^I(04/20/2021 02:43:44 PM)^I(Detached)\n\n"
                print(erm)
                with open( os.path.expanduser("~/myservice.errors","a") ) as f:
                    f.write(erm)

        if stopme:
            sys.exit(1)



        age=NOW-t   # and remove microseconds
        age= datetime.timedelta(seconds=math.ceil(age.total_seconds()))
        #
        # corellate to CONFIG
        #print(name ," / " ,session ," / ", age  ,"/" )
        if not name in g.config:
            TIMEFMT="%Y/%m/%d-%H:%M:%S"
            TIMESTR=datetime.datetime.now().strftime( TIMEFMT )
            introduce_empty_member( name , TIMESTR )
        g.config[name]['session']=session
        g.config[name]['age']=strfdelta(age)
        # last seen - record the value
        g.config[name]['lastrun']=NOW.strftime("%Y/%m/%d-%H:%M:%S")

    #print( '=============',g.config['hi_jupy'] )
    #time.sleep(4)
    return g.config








def read_myservice_config():
    """
    this reads the '~/.myservice.conf'  file
    into global  g.config
    If file not present, it creates one from '.permanent' etc.
    If even this is not present, it creates an empty one.
    """
    #global config
    g.logger.debug("reading CONFIG (read_mys_conf)"+g.MYSECONFIG)
    TIMEFMT="%Y/%m/%d-%H:%M:%S"
    CONF = g.MYSECONFIG
    if os.path.isfile( CONF ):
    # IF CONF EXISTS:
    # IF CONF EXISTS:
        #if g.DEBUG:print("i... file ",CONF,"exists, reading config")
        with open( CONF , 'r') as f:
            g.config = json.load(f)
        #this was the original--------------------------------
        toremove=[]
        for i in g.config.keys():# somewhere we forgot to define path
            if not "path" in g.config[i].keys():
                g.config[i]["path"]=""
            if g.config[i]["path"]!="": # sort path/name= define here
                g.config[i]["sortname_noedit"]=g.config[i]["path"]+"/"+i
            else:
                g.config[i]["sortname_noedit"]=i
            pname=g.config[i]["sortname_noedit"]
            if (not os.path.isfile(g.MYSEPATH+"/"+pname)) or (not  os.access(g.MYSEPATH+"/"+pname, os.X_OK)):
                g.logger.error(pname+" NOT PRESENT or NOT EXECUTABLE")
                toremove.append( i )
        if len(toremove)>0:
            for i in toremove:
                g.logger.error("removing service:"+i)
                del g.config[i]
    else:
    # CREATE IF NONE EXIST ===================
    # CREATE IF NONE EXIST ===================
        g.config={}
        # get all execs in subdir with paths !
        files=get_list_myservice_files()
        # also with PATH  TTT/aaa
        #NO?#save_myservice_config( True  )  # saves empty now ?yes!
        TIMEFMT="%Y/%m/%d-%H:%M:%S"
        TIMESTR=datetime.datetime.now().strftime( TIMEFMT )
        for i in files: # go through all executable files
            # now it is very simple like TTT/aaa
            #    i want to keep name aaa but infdisplay TTT/aaa
            #      and screen -S aaa_myservice
            introduce_empty_member( i  , TIMESTR )
            # HEY-ALL ENABLED NOW=WHY NOT
        # #### SAVE ################
        save_myservice_config( True  ) # when creating, save
    # config is saved ==== the idea about 2nd line for description:

    return g.config





def read_desc_from_files(config):
    """
Directly read from files e 2nd line and if it is
#myservice_description:
use it as a description
    """
    PATH=os.path.expanduser( '~/.myservice/')
    for i in config:
        fnam=PATH+config[i]['path']+"/"+i
        #print(i, fnam)
        b=""
        with open(fnam) as f:
            ok=False
            try:
                f.readline()
                b=f.readline().strip().rstrip()
                c=f.readline().strip().rstrip()
                ok=True
            except:
                g.logger.error("probably binary file to read")
        # SECONDLINE DESCRIPTION
        # second or THIRDLINE DOCKER
        if b.find("#myservice_description:")==0:
            b=b.split("#myservice_description:")[1]
            #print("DESCRIPTION:",b)
            config[i]['desc']=b


        if b.find("#myservice_docker_down:")==0:
            b=b.split("#myservice_docker_down:")[1]
            config[i]['dockerdown']=b
            config[i]['desc']+="(DOWN)"

        if c.find("#myservice_docker_down:")==0:
            c=c.split("#myservice_docker_down:")[1]
            config[i]['dockerdown']=c
            config[i]['desc']+="(DOWN)"


    return config






############## ALL EXECUTABLES #############
# returns list:
#  test TTT/aaa ....
def get_list_myservice_files( subpath="" ):
    """
    recursively finds EXE files.
    I think  ONE level only
    no ~, exe only, no .subdirs
    Used from read_myservice_config()
    """
    PATH=os.path.expanduser( '~/.myservice/'+subpath+"/")
    files=glob.glob( PATH+"*")
    # _F_iles  : no *~ , no .file like .git
    Files=[ i for i in files if i.find("~")<0 and i.find(".")!=0 ]
    # only exe files and files ==>>  not .git:
    files=[ i for i in Files if is_exe(i) and os.path.isfile(i) ]
    # full path added now
    files=[ os.path.basename(i) for i in files  ]
    dires=[]
    files2=[]
    if subpath=="":
        dires=[ i for i in Files if os.path.isdir(i) ]
        #remove ~/.myservice/
        dires=[ os.path.basename(i) for i in dires  ]
        # remove all starting with _   !!!
        dires=[ i for i in dires if i.find("_")!=0 ]
        #logger.debug(subpath+" GET_LIST_DIRES:\n"+"\n".join(dires) )
        ## RECURSE
        files2= [ get_list_myservice_files(i) for i in dires]

        # flaten list of lists # flatten it
        files2=[item for sublist in files2 for item in sublist]
        #logger.debug("/GET_LIST_FILE2:\n"+"\n".join(files2) )
        files=files+[ i for i in files2]
    #logger.debug(subpath+"/GET_LIST_FILES:\n"+"\n".join(files) )
    #print( 'ALLFILES IN MYSERVICE:', files )
    if subpath!="":
        files=[ subpath+"/"+i for i in files]
    return files








def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)






####################
#   in config
####################
def update_age(  ):
    #g.logger.debug( "  update age .................")
    NOW=datetime.datetime.now()
    for name in g.config.keys():
        #  first - assure that age exists
        if g.config[name]['added']=="":
            g.config[name]['added']="2018/04/30-10:00:00"
        if g.config[name]['lastrun']=="":
            g.config[name]['lastrun']=g.config[name]['added']
        lastrun=g.config[name]['lastrun']
        t=datetime.datetime.strptime( lastrun, "%Y/%m/%d-%H:%M:%S")
        age=NOW-t
        age= datetime.timedelta(seconds=math.ceil(age.total_seconds()))
        g.config[name]['age']=strfdelta(age)
        #if  g.config[name]['lastrun']!="":g.config[name]['lastrun']=NOW
        #g.logger.debug( "  age /" +g.config[name]['age'] +"/")

# i cannot change imutable oblejt ==dict. I must go GLOBAL
#   AGE




if __name__=="__main__":
    print("================ SUBFUNCTION mysconfig===============")
    a=read_myservice_config()
    #print(a)
    c=read_desc_from_files(a)
    print(c)
    #read_myservice_config()
    #print( g.config )
