#!/usr/bin/env python3
import zmq
import json
import time
import sys
import datetime
import argparse
import socket
import os
import uuid  # not too much useful here
import signal
from threading import Thread  # thread

import re

import gevent
EXITME=False  # REP exit way with ctrl-c


system_name=socket.gethostname()
user_name = os.getenv('USER')
if user_name==None:
    user_name="none"
#print(user_name, system_name)


parser=argparse.ArgumentParser(description="""
     The program sends a JSON message to different machines, different ports

- the delay between two send_request is default to 0.1 second
- the timout is 1 second (and after all threads join)
- it can repeat send_req (testing ?)
- PUB - SUB added too
""",usage="",
 formatter_class=argparse.RawTextHelpFormatter)



parser.add_argument('-d','--debug', action='store_true' )
parser.add_argument('-u','--usage', action='store_true' , help="HELP PAGE FOR USAGE")


parser.add_argument('-a','--actas', default='REQ', help="REQ REP  ... SUB PUB, clients=REQ,SUB; servers=REP,PUB" )

parser.add_argument('-t','--targets', action='store' , default="127.0.0.1:5678",help='target servers with ports, comma separated')
parser.add_argument('-m','--message', action='store' , default="",help='')
parser.add_argument('-s','--service', action='store' , default="",help='service to run')
parser.add_argument('-p','--parameter', action='store' , default="",help='service parameter')

parser.add_argument('-r','--reprocess', default="",help='reprocess SUB data - specify single DICT ITEM: -r tabledata,"temp",0')

parser.add_argument('-n','--repeat_n_times', type=int,action='store',default=1 , help='repeat n times ')

parser.add_argument('-w','--wait_seconds', type=float ,action='store', default=0 , help='wait w seconds before repeat ')

parser.add_argument('-x','--xxx_replace', action='store_true' , help='if --json has "xxx" in it , it will be replaced by STDIN value; ttt will be replaced by time ')

parser.add_argument('-j','--json', default='', help='define the JSON string, double quotes inside, single outer quotes: \'{"tabledata":{"1min":["xxx","ttt", "red"]}\'' )


args=parser.parse_args()

if args.usage:
    print( """
############################################################
              Usage examples:
############################################################

=========================================================
==== REQ ==== message to REP port, answer awaited

SEND a JSON message (-m) to a TARGET and ask to run myservice service 'tarme' (-s)
   nczmq.py -t 127.0.0.1:9000 -m "starting tarme prog." -s tarme

SEND to multiple TARGETS
   nczmq.py -t 1.2.3.4:5000,127.0.0.1:9000 -m "everythong is ok" -s someservice

SEND 10 times, wait 5 seconds between two sends
   nczmq.py -t 127.0.0.1:9000 -m "starting tarme prog." -s tarme -n 10 -w 5
      send 10x and wait 5 seconds between repetions

=========================================================
==== PUB SUB ==== message from PUB port no answer awaited

SEND LOAD AVERAGES:

  for ((i=1;i<10000;i++)); do cat /proc/loadavg | cut -d " " -f 1-3; sleep 1;done  | ./nczmq.py -a PUB  -t 26106 --json '{"tabledata":{"1min":["xxx","ttt", "red"],"5min":["xxx","ttt","green"],"15min":["xxx","tt,"yellow"]}}' -d -n 11111 -w 1 -x
  # - put sleep 1;  on input to cope with -w in output
  # the PUB publishes data for web socket with javascript graphs


READ TEMPERATURES from sermas:
   nczmq.py -a SUB -t 127.0.0.1:26106 -r tabledata,"temp",0  | toilet -f bigmono12

""")
    quit()


if args.debug:print("if using with myservice:   nczmq.py -s service -p parameter")

ports=[]  # THIS IS TARGET LIST ================
if len(args.targets)>0:
    ports=args.targets.split(",")

# case of a server :===============
# PUB is server
# REP is server
if (args.actas=='PUB') and ((args.repeat_n_times<=1 ) or (args.wait_seconds<0.2)):
    print("i... PUB will require more repetitions and timeouts. Try  -n 2 -w 1")
if args.actas=='PUB' or args.actas=='REP':
    if ports[0].find(":")>=0:
        print("x...  only port number is required for", args.actas)
        PORT=ports[0].split(":")[1]
    else:
        PORT=ports[0]
    BINDSTR="tcp://0.0.0.0:{}".format( PORT )
else:
    BINDSTR="tcp://{}".format(ports[0])



#print(args.targets, ports)
#quit()
################
#
#  connect to seeral sockets and send the data
#
################
#ports = ["127.0.0.1:5678","127.0.0.1:5678" ]
#ports = ["127.0.0.1:5678"]

MESSAGE=args.message
SERVICE=args.service
PARAMETER=args.parameter

if args.debug:print('TARGETS==',ports)



def connectall( socket , port):
    if args.debug: print("Connecting to machine... %s" % port)
    #for port in ports:
    socket.connect("tcp://%s" % port)
    if args.debug:print("socket connected to %s" % port)




def receive_reply( i, request,socket  ):
    # use poll for timeouts:
    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)

    socks = dict(poller.poll(1 * 1000)) # 1 sec
    if args.debug:print(i,request,"    +waiting response...")
    if socket in socks:
        try:
            msg_json = socket.recv_json()
            if args.debug:print(i,request,"    Received reply ", request, "[", msg_json, "]")
        except IOError:
            if args.debug:print(i,request,"  !Could not connect to machine")
    else:
        if args.debug:print(i,request,"    !Machine did not respond ... ")




###########################################
#
# MAIN
#
###########################################

sockets=[]


if args.actas=="REQ":  # client of REQ-REP ,  sends
    context = zmq.Context()
    sockets=[]
    # fills sockets
    # connectrs to TCP
    for i in range(len(ports)):
        socket = context.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, 0)
        sockets.append( socket )
        connectall( socket, ports[i] )

    i=0
    while True:
        i=i+1
        #if i>args.repeat_n_times: break

        threads=[]   # wait response in threads

        for request in range(len(ports)):

            now=datetime.datetime.now().strftime("%H:%M:%S")
            day=datetime.datetime.now().strftime("%Y/%m/%d")
            JS= {'n':i,'worker':request,
                 'message':MESSAGE,
                 'time':now,
                 'date':day,
                 'run':SERVICE, # i will remove this
                 'service':SERVICE,
                 'parameter':PARAMETER,
                 'from':user_name+"@"+system_name}
            if args.debug:print("=================================================")
            if args.debug:print(i,"-----Sending REQ --- to",ports[request],"-------\n  ",JS)
            try:
                sockets[request].send_json( JS )  # <-- error occurs here
                #print("          [ok]")  # this will get printed
            except:
                if args.debug:print("         !  error sending json")
                sockets[request] = context.socket(zmq.REQ)
                sockets[request].setsockopt(zmq.LINGER, 0)
                connectall( sockets[request], ports[request] )

            ttt=Thread(target=receive_reply,args=(i,request, sockets[request] ))
            threads.append(  ttt )
            threads[-1].start()
            time.sleep(0.1)

        for j in threads: j.join()
        if args.debug:print("...................... JOINED.................")
        if i>=args.repeat_n_times: break
        #if len(MESSAGE)>0:
        time.sleep( args.wait_seconds)





########### NICE END of program ##
#
#  CTRL C
#
####################################
RANDOM_STR = uuid.uuid4()

def signal_handler(signal, frame):
    EXITME=True   # this will be the main reason
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.setsockopt(zmq.LINGER, 0)
    if BINDSTR.find("0.0.0.0")>0:
        BINDSTR=BINDSTR.replace("0.0.0.0","127.0.0.1")
    socket.connect( BINDSTR )
    socket.send_json( {'secret':str(RANDOM_STR),'message':'kill'} )
    sys.exit(0)
#####################################





#####################################
#
#      PORT BIND
#
#####################################
if args.actas=="REP":  # server of REQ-REP      receives
    #==============
    signal.signal(signal.SIGINT, signal_handler) #  CTRL-C : Legal quit


    context = zmq.Context()
    sock = context.socket(zmq.REP)
    # create bind socket
    if args.debug:print( "i... ",BINDSTR )
    res=False
    try:
        sock.bind( BINDSTR )
        res=True
    except:
        print("X... error in binding the port")
    if not res:quit()
    if args.debug:print("i... bound ",BINDSTR)
    i=0
    while True:
        i=i+1
        message = sock.recv_json()
        print(message)
        sock.send_json( {} )
        if EXITME:quit


def isfloat( line ):
    xisfloat=False
    try:
        float(line)
        xisfloat=True
    except:
        xisfloat=False
    return xisfloat


def isint( line ):
    xisfloat=False
    try:
        int(line)
        xisfloat=True
    except:
        xisfloat=False
    return xisfloat


#================= GOOD FOR PUB ========
# replacing uccurences of xxx by
def callback(match):
    return next(callback.v)
#callback.v=iter(('1','2','3'))

#####################################
#
#      PORT BIND  PUBlisher BUT it is in fact client==sender
#
#####################################
if args.actas=="PUB":  #  PUB - SUB    sends
    #==============
    #signal.signal(signal.SIGINT, signal_handler) #  CTRL-C : Legal quit

    print("I... PUB WAITS FOR THE COMMAND INPUT")
    import sys

    context = zmq.Context()
    sock = context.socket(zmq.PUB)
    # create bind socket
    if args.debug:print( "i... ",BINDSTR )
    res=False
    try:
        sock.bind( BINDSTR )
        res=True
    except:
        print("X... error in binding the port")
    if not res:quit()
    if args.debug:print("i... bound ",BINDSTR)
    i=0
    while True:
        #line="xxx" # nonsense
        if args.xxx_replace:  #==== Yes, I WANT REPLACEMENTS
            line=sys.stdin.readline()
            if line=="":quit()  #ctrld
            line=line.strip()
            print("OBTAINED: /{}/".format(line) )
            isfloat=False
            try:
                float(line)
                isfloat=True
            except:
                isfloat=False
            if not isfloat:
                #print("IT IS NOT ONE NUMBER")
                if line.find(",")>0:
                    vals=line.split(",")
                else:
                    vals=line.split(" ")
                #print("splited:",vals)
            else:
                vals=[line]
            callback.v=iter(vals)

        # OR - NO REPLACEMENTS, JSUT STATIC JSON
        i=i+1
        dict={"n":i}
        if len(MESSAGE)>0:
            dict["message"]=MESSAGE
        if len(SERVICE)>0:
            dict["run"]=SERVICE
        if len(PARAMETER)>0:
            dict["parameter"]=PARAMETER
        dict["from"]=user_name+"@"+system_name
        now=datetime.datetime.now().strftime("%H:%M:%S")
        day=datetime.datetime.now().strftime("%Y/%m/%d")
        dict["time"]=now
        dict["date"]=day
        # -j   option ======================
        if len(args.json)>0: # There is JSON option   -j
            a=args.json # take the json STRING and
            if args.xxx_replace:
                a=re.sub(r'xxx',callback, a) # replace all xxx
                print("replacing ttt")
                a=a.replace("ttt", now ) # replace all xxx
            DICTJSON=json.loads( a )
            dict=DICTJSON
            #a=json.dumps( DICTJSON ) # not necessary now
            #a=a.replace("xxx",line)  # done already
            #dict=json.loads(a)       # not necessary now
        print( "sending json:", dict )
        sock.send_json( dict )
        if EXITME:quit()
        time.sleep( args.wait_seconds )
        if i>=args.repeat_n_times: quit()







def procsubdata( data ):
    """
    access the DICT that is sent in json format
    and print it
    """
    #da = json.loads(data)
    roll="\n"
    rollin=" \n"*3
    if args.reprocess.find(",")>0:
        #print(chr(27) + "[2J", end="")#clear
        #import os
        #os.system('cls' if os.name == 'nt' else 'clear')
        #import sys
        #sys.stderr.write("\x1b[2J\x1b[H")
        #print("\033[0;1f", end="")
        #print("\033\143", end="")
        #print("\33[H\33[2J", end="")  # clear
        #print("\33[0;34m\]", end="") # blue
        f=args.reprocess.split(",")
        for i in range(len(f)):
            if isint( f[i] ):
                f[i]=int(f[i])
        if len(f)==2:
            print(rollin, data[ f[0] ][ f[1] ] , end=roll ,flush=True)
        if len(f)==3:
            #VERT="0"
            #HORIZ="" "\033["+VERT+";"+HORIZ+"f",
            print( rollin, data[ f[0] ][ f[1] ][ f[2] ] , end=roll,  flush=True)
    else:
        print( rollin, data[args.reprocess] , end=roll ,flush=True)




#####################################
#
# SUBscriber (receiver) :  attaches to PUBlisher
#
#####################################
if args.actas=="SUB":  #  PUB - SUB
    #==============
    #signal.signal(signal.SIGINT, signal_handler) #  CTRL-C : Legal quit

    context = zmq.Context()
    sock = context.socket( zmq.SUB )
    # create bind socket
    if BINDSTR.find("0.0.0.0")>0:
        BINDSTR=BINDSTR.replace("0.0.0.0","127.0.0.1")
    if args.debug:print( "i... ",BINDSTR )
    res=False
#    try:
    sock.connect( BINDSTR )
    sock.setsockopt_string(zmq.SUBSCRIBE, "")
    poller = zmq.Poller()
    poller.register(sock, zmq.POLLIN)

    res=True
#    except:
#        print("X... error in binding the port")
    if not res:quit()
    if args.debug:print("i... bound ",BINDSTR)
    i=0
    while True:
        i=i+1
        data = sock.recv_json()
        if args.reprocess!="":
            procsubdata( data )
        else:
            print(data)
        if EXITME:quit()
        #if i>10: quit()
        gevent.sleep()
